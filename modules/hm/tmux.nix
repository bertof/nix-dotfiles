{ pkgs, ... }:
{
  programs.tmux = {
    enable = true;
    clock24 = true;
    escapeTime = 25;
    terminal = "screen-256color";
    plugins = builtins.attrValues { inherit (pkgs.tmuxPlugins) nord prefix-highlight; };
    extraConfig = ''
      set -g mouse on
    '';
  };
}
