{ pkgs, ... }:
{
  home = {
    packages = [
      pkgs.ffmpegthumbnailer
      pkgs.exiftool
      pkgs.mediainfo
      pkgs.unar
      pkgs.mpv
    ];
    shellAliases.y = "yazi";
  };

  programs.yazi = {
    enable = true;
    enableBashIntegration = true;
    enableNushellIntegration = true;
    enableZshIntegration = true;
    settings = {
      manager = {
        sort_by = "natural";
        sort_dir_first = true;
        sort_reverse = false;
      };

      keymap = [
        { exec = "<C-s>"; on = [ "shell zsh --block --confirm" ]; }
      ];

      opener = {
        folder = [
          { run = ''nemo "$@"''; display_name = "Nemo"; }
          { run = ''xdg-open "$@"''; display_name = "Open folder"; }
          { run = ''$VISUAL "$@"''; }
        ];
        archive = [
          { run = ''file-roller "$1"''; display_name = "Open with File Roller"; }
          { run = ''unar "$1"''; display_name = "Extract here"; }
          { run = ''xdg-open "$@"''; display_name = "Open with default"; }
          { run = ''$VISUAL "$@"''; }
        ];
        text = [
          { run = ''$VISUAL "$@"''; }
          { run = ''nvim "$@"''; block = true; display_name = "Open with NeoVIM"; }
          { run = ''hx "$@"''; block = true; display_name = "Open with Helix"; }
          { run = ''bat --pagin=always "$@"''; block = true; display_name = "Open with bat"; }
        ];
        image = [
          { run = ''xdg-open "$@"''; display_name = "Open"; }
          { run = ''eog "$@"''; display_name = "Open with EOG"; }
          { run = '''exiftool "$1"; echo "Press enter to exit"; read''; block = true; display_name = "Show EXIF"; }
        ];
        video = [
          { run = ''mpv "$@"''; display_name = "Open with MPV"; }
          { run = ''umpv "$@"''; display_name = "Open with uMPV"; }
          { run = ''totem "$@"''; display_name = "Open with Totem"; }
          { run = ''mediainfo "$1"; echo "Press enter to exit"; read''; block = true; display_name = "Show media info"; }
          { run = ''xdg-open "$@"''; display_name = "Open with default"; }
          { run = ''$VISUAL "$@"''; }
        ];
        audio = [
          { run = ''mpv "$@"''; display_name = "Open with MPV"; }
          { run = ''umpv "$@"''; display_name = "Open with uMPV"; }
          { run = '''mediainfo "$1"; echo "Press enter to exit"; read''; block = true; display_name = "Show media info"; }
          { run = ''xdg-open "$@"''; display_name = "Open with default"; }
          { run = ''$VISUAL "$@"''; }
        ];
        fallback = [{ run = ''xdg-open "$@"''; display_name = "Open"; }
          { run = ''bat --pagin=always "$@"''; block = true; display_name = "Open with bat"; }
          { run = ''$VISUAL "$@"''; block = true; display_name = "Open with editor"; }
          { run = ''$EDITOR "$@"''; block = true; display_name = "Open with editor"; }];
      };
    };
  };
}
