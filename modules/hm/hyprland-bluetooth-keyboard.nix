{ pkgs
, lib
, nixosConfig
, ...
}:
{
  systemd.user.services."bluetooth-keyboard" = {
    Unit = {
      Description = "Disable the laptop keyboard when the bluetooth keyboard is connected";
      After = [ "graphical-session.target" ];
      PartOf = [ "graphical-session.target" ];
    };
    Install.WantedBy = [ "graphical-session.target" ];
    Service = {
      Type = "simple";
      ExecStart = pkgs.writeShellScript "bluetooth-keyboard.sh" ''
        PATH=$PATH:${
          lib.makeBinPath [
            pkgs.coreutils
            pkgs.gnugrep
            pkgs.libnotify
            nixosConfig.programs.hyprland.package
          ]
        }
        BLUETOOTH_DEVICE="keychron-k1-max-keyboard"
        LAPTOP_DEVICE="at-translated-set-2-keyboard"
        STATUS_FILE="$XDG_RUNTIME_DIR/bluetooth_keyboard.status"

        enable_laptop_keyboard() {
          if [[ `cat $STATUS_FILE` == "false" ]]; then
            printf "true" > "$STATUS_FILE"
            echo "Enabling laptop keyboard"
            notify-send -u normal "Enabling laptop keyboard"
            hyprctl keyword '$LAPTOP_KEYBOARD_ENABLED' true
            hyprctl keyword device:a ""
          fi
        }

        disable_laptop_keyboard() {
          if [[ `cat $STATUS_FILE` == "true" ]]; then
            printf "false" > "$STATUS_FILE"
            echo "Disabling laptop keyboard"
            notify-send -u normal "Disabling laptop keyboard"
            hyprctl keyword '$LAPTOP_KEYBOARD_ENABLED' false
            hyprctl keyword device:a ""
          fi
        }

        printf "true" > "$STATUS_FILE"

        while true; do
          if hyprctl devices | grep --quiet $BLUETOOTH_DEVICE ; then
            disable_laptop_keyboard
          else
            enable_laptop_keyboard
          fi

          sleep 5;
        done
      '';
    };
  };
}
