{ pkgs, ... }:
let
  megasyncCmd = "${pkgs.megacmd}/bin/mega-cmd-server";
in
{
  home.packages = [ pkgs.megacmd ];

  systemd.user.services."mega-cmd-server" = {
    Unit = {
      Description = "Mega command server";
      After = [ "default.target" ];
      PartOf = [ "default.target" ];
    };
    Install = {
      WantedBy = [ "default.target" ];
    };
    Service = {
      Type = "simple";
      ExecStart = megasyncCmd;
    };
  };
}
