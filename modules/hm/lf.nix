{ pkgs, ... }:
let
  cleaner = pkgs.writeShellScript "cleaner" ''
    kitty +kitten icat --clear --stdin no --silent --transfer-mode file < /dev/null > /dev/tty
  '';

  previewer = pkgs.writeShellScript "previewer" ''
    file=$1
    w=$2
    h=$3
    x=$4
    y=$5

    preview() {
        kitty +kitten icat --silent --stdin no --transfer-mode file --place "''${w}x''${h}@''${x}x''${y}" "$1" < /dev/null > /dev/tty
    }

    TEMPDIR=~/.cache/lf

    file="$1"; shift
    case "$(basename "$file" | tr '[A-Z]' '[a-z]')" in
      *.tar*) tar tf "$file" ;;
      *.zip) unzip -l "$file" ;;
      *.rar) unrar l "$file" ;;
      *.7z) 7z l "$file" ;;
      *.avi|*.mp4|*.mkv)
      	thumbnail="$TEMPDIR/thumbnail.png"
      	ffmpeg -y -i "$file" -vframes 1 "$thumbnail"
          preview "$thumbnail"
      	;;
      *.pdf)
      	thumbnail="$TEMPDIR/thumbnail.png"
      	gs -o "$thumbnail" -sDEVICE=pngalpha -dLastPage=1 "$file" >/dev/null
          preview "$thumbnail"
      	;;
      *.jpg|*.jpeg|*.png|*.bmp)
          preview "$file"
          ;;
      *.ttf|*.otf|*.woff)
      	thumbnail="$TEMPDIR/thumbnail.png"
      	fontpreview -i "$file" -o "$thumbnail"
      	preview "$thumbnail"
      	;;
      *.svg)
      	thumbnail="$TEMPDIR/thumbnail.png"
      	convert "$file" "$thumbnail"
      	preview "$thumbnail"
      	;;
      *) lf_bat_preview "$file" ;;
    esac
    return 127 # nonzero retcode required for lf previews to reload
  '';
in
{
  programs.lf = {
    enable = true;
    settings = {
      dircounts = true;
      icons = true;
      # mouse = true;
      number = true;
      # scrollof = 3;
      hidden = true;
      ignorecase = true;
      tabstop = 2;
    };
    previewer = {
      source = previewer;
      # keybinding = "i";
    };
    keybindings = {
      "<c-f>" = "$EDITOR $(fzf)";
    };
    commands = {
      mkdir = ''
        ''${{
                printf "Directory name: "
                read ans
                mkdir $ans
              }}'';
      mkfile = ''
        ''${{
                printf "File name: "
                read ans
                $VISUAL $ans
              }}'';

      trash = ''
        ''${{
                files=$(printf "$fx" | tr '\n' ';')
                while [ "$files" ]; do
                  file=''${files%%;*}

                  ${pkgs.trash-cli}/bin/trash-put "$(basename "$file")"
                  if [ "$files" = "$file" ]; then
                    files=""
                  else
                    files="''${files#*;}"
                  fi
                done
              }}'';
      restore_trash = ''
        ''${{
                trash-restore
              }}'';

      z = ''
        ''${{
                result="$(zoxide query --exclude "$PWD" -- "$@")"
                lf -remote "send $id cd '$result'"
              }}'';
      zi = ''
        ''${{
                result="$(zoxide query -i -- "$@")"
                lf -remote "send $id cd '$result'"     
              }}'';
      extract = ''
        ''${{
                set -f
                case $f in
                  *.tar.bz|*.tar.bz2|*.tbz|*.tbz2) tar xjvf $f;;
                  *.tar.gz|*.tgz) tar xzvf $f;;
                  *.tar.xz|*.txz) tar xJvf $f;;
                  *.zip) unzip $f;;
                  *.rar) unrar x $f;;
                  *.7z) 7z x $f;;
                esac
              }}'';
      "tar" = ''
        ''${{
                set -f
                mkdir $1
                cp -r $fx $1
                tar czf $1.tar.gz $1
                rm -rf $1
              }}'';
      "zip" = ''
        ''${{
                set -f
                mkdir $1
                cp -r $fx $1
                zip -r $1.zip $1
                rm -rf $1
              }}'';
    };
    extraConfig = ''
      set cleaner ${cleaner}
    '';
  };

  xdg.configFile."lf/icons".text = ''
    # vim:ft=conf

    # These examples require Nerd Fonts or a compatible font to be used.
    # See https://www.nerdfonts.com for more information.

    # default values from lf (with matching order)
    # ln      l       # LINK
    # or      l       # ORPHAN
    # tw      t       # STICKY_OTHER_WRITABLE
    # ow      d       # OTHER_WRITABLE
    # st      t       # STICKY
    # di      d       # DIR
    # pi      p       # FIFO
    # so      s       # SOCK
    # bd      b       # BLK
    # cd      c       # CHR
    # su      u       # SETUID
    # sg      g       # SETGID
    # ex      x       # EXEC
    # fi      -       # FILE

    # file types (with matching order)
    ln             # LINK
    or             # ORPHAN
    tw      t       # STICKY_OTHER_WRITABLE
    ow             # OTHER_WRITABLE
    st      t       # STICKY
    di             # DIR
    pi      p       # FIFO
    so      s       # SOCK
    bd      b       # BLK
    cd      c       # CHR
    su      u       # SETUID
    sg      g       # SETGID
    ex             # EXEC
    fi             # FILE

    # file extensions (vim-devicons)
    *.styl          
    *.sass          
    *.scss          
    *.htm           
    *.html          
    *.slim          
    *.haml          
    *.ejs           
    *.css           
    *.less          
    *.md            
    *.mdx           
    *.markdown      
    *.rmd           
    *.json          
    *.webmanifest   
    *.js            
    *.mjs           
    *.jsx           
    *.rb            
    *.gemspec       
    *.rake          
    *.php           
    *.py            
    *.pyc           
    *.pyo           
    *.pyd           
    *.coffee        
    *.mustache      
    *.hbs           
    *.conf          
    *.ini           
    *.yml           
    *.yaml          
    *.toml          
    *.bat           
    *.mk            
    *.jpg           
    *.jpeg          
    *.bmp           
    *.png           
    *.webp          
    *.gif           
    *.ico           
    *.twig          
    *.cpp           
    *.c++           
    *.cxx           
    *.cc            
    *.cp            
    *.c             
    *.cs            
    *.h             
    *.hh            
    *.hpp           
    *.hxx           
    *.hs            
    *.lhs           
    *.nix           
    *.lua           
    *.java          
    *.sh            
    *.fish          
    *.bash          
    *.zsh           
    *.ksh           
    *.csh           
    *.awk           
    *.ps1           
    *.ml            λ
    *.mli           λ
    *.diff          
    *.db            
    *.sql           
    *.dump          
    *.clj           
    *.cljc          
    *.cljs          
    *.edn           
    *.scala         
    *.go            
    *.dart          
    *.xul           
    *.sln           
    *.suo           
    *.pl            
    *.pm            
    *.t             
    *.rss           
    '*.f#'          
    *.fsscript      
    *.fsx           
    *.fs            
    *.fsi           
    *.rs            
    *.rlib          
    *.d             
    *.erl           
    *.hrl           
    *.ex            
    *.exs           
    *.eex           
    *.leex          
    *.heex          
    *.vim           
    *.ai            
    *.psd           
    *.psb           
    *.ts            
    *.tsx           
    *.jl            
    *.pp            
    *.vue           ﵂
    *.elm           
    *.swift         
    *.xcplayground  
    *.tex           ﭨ
    *.r             ﳒ
    *.rproj         鉶
    *.sol           ﲹ
    *.pem           

    # file names (vim-devicons) (case-insensitive not supported in lf)
    *gruntfile.coffee       
    *gruntfile.js           
    *gruntfile.ls           
    *gulpfile.coffee        
    *gulpfile.js            
    *gulpfile.ls            
    *mix.lock               
    *dropbox                
    *.ds_store              
    *.gitconfig             
    *.gitignore             
    *.gitattributes         
    *.gitlab-ci.yml         
    *.bashrc                
    *.zshrc                 
    *.zshenv                
    *.zprofile              
    *.vimrc                 
    *.gvimrc                
    *_vimrc                 
    *_gvimrc                
    *.bashprofile           
    *favicon.ico            
    *license                
    *node_modules           
    *react.jsx              
    *procfile               
    *dockerfile             
    *docker-compose.yml     
    *rakefile               
    *config.ru              
    *gemfile                
    *makefile               
    *cmakelists.txt         
    *robots.txt             ﮧ

    # file names (case-sensitive adaptations)
    *Gruntfile.coffee       
    *Gruntfile.js           
    *Gruntfile.ls           
    *Gulpfile.coffee        
    *Gulpfile.js            
    *Gulpfile.ls            
    *Dropbox                
    *.DS_Store              
    *LICENSE                
    *React.jsx              
    *Procfile               
    *Dockerfile             
    *Docker-compose.yml     
    *Rakefile               
    *Gemfile                
    *Makefile               
    *CMakeLists.txt         

    # file patterns (vim-devicons) (patterns not supported in lf)
    # .*jquery.*\.js$         
    # .*angular.*\.js$        
    # .*backbone.*\.js$       
    # .*require.*\.js$        
    # .*materialize.*\.js$    
    # .*materialize.*\.css$   
    # .*mootools.*\.js$       
    # .*vimrc.*               
    # Vagrantfile$            

    # file patterns (file name adaptations)
    *jquery.min.js          
    *angular.min.js         
    *backbone.min.js        
    *require.min.js         
    *materialize.min.js     
    *materialize.min.css    
    *mootools.min.js        
    *vimrc                  
    Vagrantfile             

    # archives or compressed (extensions from dircolors defaults)
    *.tar   
    *.tgz   
    *.arc   
    *.arj   
    *.taz   
    *.lha   
    *.lz4   
    *.lzh   
    *.lzma  
    *.tlz   
    *.txz   
    *.tzo   
    *.t7z   
    *.zip   
    *.z     
    *.dz    
    *.gz    
    *.lrz   
    *.lz    
    *.lzo   
    *.xz    
    *.zst   
    *.tzst  
    *.bz2   
    *.bz    
    *.tbz   
    *.tbz2  
    *.tz    
    *.deb   
    *.rpm   
    *.jar   
    *.war   
    *.ear   
    *.sar   
    *.rar   
    *.alz   
    *.ace   
    *.zoo   
    *.cpio  
    *.7z    
    *.rz    
    *.cab   
    *.wim   
    *.swm   
    *.dwm   
    *.esd   

    # image formats (extensions from dircolors defaults)
    *.jpg   
    *.jpeg  
    *.mjpg  
    *.mjpeg 
    *.gif   
    *.bmp   
    *.pbm   
    *.pgm   
    *.ppm   
    *.tga   
    *.xbm   
    *.xpm   
    *.tif   
    *.tiff  
    *.png   
    *.svg   
    *.svgz  
    *.mng   
    *.pcx   
    *.mov   
    *.mpg   
    *.mpeg  
    *.m2v   
    *.mkv   
    *.webm  
    *.ogm   
    *.mp4   
    *.m4v   
    *.mp4v  
    *.vob   
    *.qt    
    *.nuv   
    *.wmv   
    *.asf   
    *.rm    
    *.rmvb  
    *.flc   
    *.avi   
    *.fli   
    *.flv   
    *.gl    
    *.dl    
    *.xcf   
    *.xwd   
    *.yuv   
    *.cgm   
    *.emf   
    *.ogv   
    *.ogx   

    # audio formats (extensions from dircolors defaults)
    *.aac   
    *.au    
    *.flac  
    *.m4a   
    *.mid   
    *.midi  
    *.mka   
    *.mp3   
    *.mpc   
    *.ogg   
    *.ra    
    *.wav   
    *.oga   
    *.opus  
    *.spx   
    *.xspf  

    # other formats
    *.pdf   
  '';
}
