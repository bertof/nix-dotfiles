{ pkgs, nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  inherit (nixosConfig.networking) hostName;
  inherit (nixosConfig.nix-rice.rice) colorPalette font;
  alpha = 0.9;
  custom = palette.toRGBHex {
    inherit (colorPalette.primary) background foreground;
    alpha_background = color.setAlphaRgba alpha colorPalette.primary.background;
    primary_accent = colorPalette.normal.blue;
    secondary_accent = colorPalette.normal.cyan;
    tertiary_accent = colorPalette.primary.yellow;
    palette = {
      primary_background_rgba = colorPalette.primary.background;
      tertiary_background_hex = color.setAlphaRgba alpha colorPalette.primary.background;
    };
  };
  swaync-client = "${pkgs.swaynotificationcenter}/bin/swaync-client";
in
{
  systemd.user.services.waybar = {
    Unit.PartOf = [ "tray.target" ];
    Install.WantedBy = [ "tray.target" ];
    Service = { Restart = "on-failure"; RestartSec = "3s"; };
  };
  home.packages = [ pkgs.swaynotificationcenter ];
  programs.waybar = {
    enable = true;
    systemd.enable = true;
    systemd.target = "hyprland-session.target";
    settings = {
      mainBar = {
        layer = "top";
        height = 22;
        modules-left = [
          "hyprland/workspaces"
          "hyrpland/window"
        ];
        modules-right = [
          # "hyprland/language"
          # "custom/media"
          "pulseaudio"
          # "network"
          "cpu"
          "memory"
          "temperature"
          "keyboard-state"
          "backlight"
          "idle_inhibitor"
          "battery"
          # "battery#bat2"
          "clock"
          "custom/notification"
          "tray"
        ];

        "custom/notification" = {
          tooltip = "false";
          format = "{} {icon}";
          format-icons = {
            notification = "";
            none = "";
            dnd-notification = "";
            dnd-none = "";
          };
          return-type = "json";
          # exec-if = "which swaync-client";
          exec = "${swaync-client} -swb";
          on-click = "${swaync-client} -t -sw";
          on-click-right = "${swaync-client} -d -sw";
          escape = true;
        };

        "keyboard-state" = {
          # "numlock" = true;
          "capslock" = true;
          "format" = "{name} {icon}";
          "format-icons" = {
            "locked" = "";
            "unlocked" = "";
          };
        };
        "idle_inhibitor" = {
          "format" = "{status} {icon}";
          "format-icons" = {
            "activated" = "";
            "deactivated" = "";
          };
        };
        tray.spacing = 10;
        clock = {
          "interval" = 1;
          "format" = "{:%H:%M}";
          "tooltip" = true;
          "tooltip-format" = "{:%a %d/%m/%y}";
          # on-click = "mode";
          # tooltip = true;
          # tooltip-format = "<big>{%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
          # format = "{:%H:%M}";
          # format = "{%H:%M}";
          # format-alt = "{:%Y-%m-%d %H:%M}";
        };
        "cpu" = {
          "format" = "{usage}% ";
          "tooltip" = false;
        };
        "memory" = {
          "format" = "{}% ";
        };
        "temperature" = {
          # "thermal-zone"= 2;
          # "hwmon-path"= "/sys/class/hwmon/hwmon2/temp1_input";
          "critical-threshold" = 90;
          "format" = "{temperatureC}°C {icon}";
          "format-icons" = [ "" ];
        } // (if hostName == "sif" then {
          "hwmon-path" = "/sys/class/hwmon/hwmon7/temp1_input";
        } else { });
        "backlight" = {
          # "device"= "acpi_video1";
          "format" = "{percent}% {icon}";
          "format-icons" = [
            ""
            ""
            ""
            ""
            ""
            ""
            ""
            ""
            ""
          ];
        };
        "battery" = {
          "states" = {
            # "good"= 95;
            "warning" = 30;
            "critical" = 15;
          };
          "format" = "{capacity}% {icon}";
          "format-charging" = "{capacity}% {icon}";
          "format-plugged" = "{capacity}% ";
          "format-alt" = "{time} {icon}";
          # "format-good"= ""; # An empty format will hide the module
          # "format-full"= "";
          "format-icons" = [
            ""
            ""
            ""
            ""
            ""
          ];
        };
        # "battery#bat2" = { "bat" = "BAT2"; };
        "network" = {
          # "interface"= "wlp2*"; # (Optional) To force the use of this interface
          "format-wifi" = "{essid} ({signalStrength}%) ";
          "format-ethernet" = "{ipaddr}/{cidr} ";
          "tooltip-format" = "{ifname} via {gwaddr} ";
          "format-linked" = "{ifname} (No IP) ";
          "format-disconnected" = "Disconnected ⚠";
          "format-alt" = "{ifname}= {ipaddr}/{cidr}";
        };
        "pulseaudio" = {
          # "scroll-step"= 1; # %; can be a float
          "format" = "{volume}% {icon} {format_source}";
          "format-bluetooth" = "{volume}% {icon} {format_source}";
          "format-bluetooth-muted" = " {icon} {format_source}";
          "format-muted" = " {format_source}";
          "format-source" = "{volume}% ";
          "format-source-muted" = "";
          "format-icons" = {
            "headphone" = "";
            "hands-free" = "";
            "headset" = "";
            "phone" = "";
            "portable" = "";
            "car" = "";
            "default" = [
              ""
              ""
              ""
            ];
          };
          "on-click" = "${pkgs.pavucontrol}/bin/pavucontrol";

          "ignored-sinks" = [ "Easy Effects Sink" ];
          "ignored-sources" = [ "Easy Effects Source" ];
        };
      };
    };
    style = ''
      * {
          border: none;
          border-radius: 0px;
          font-family: ${font.monospace.name};
          font-size: 10px;
          min-height: 0;
      }
      window#waybar {
          background: ${custom.alpha_background};
      }
      #workspaces {
          background: ${custom.background};
          margin: 0 5px;
          padding: 0.5em;
          border-radius: 0.5em;
          color: ${custom.primary_accent}
      }
      #workspaces button {
          padding: 0.5em;
          margin: 0px 3px;
          border-radius: 0.5em;
          color: ${custom.primary_accent};
          background: ${custom.background};
          transition: all 0.3s ease-in-out;
      }

      #workspaces button.active {
          background-color: ${custom.secondary_accent};
          color: ${custom.background};
          border-radius: 0.5em;
          min-width: 3em;
          background-size: 400% 400%;
          transition: all 0.3s ease-in-out;
      }

      #workspaces button:hover {
          background-color: ${custom.foreground};
          color: ${custom.background};
          border-radius: 0.5em;
          min-width: 3em;
          background-size: 400% 400%;
      }

      #tray, #pulseaudio, #network, #language, #custom-notification {
          background: ${custom.background};
          font-weight: bold;
          font-size: 10px;
          margin: 0px 5px;
      }
      #tray, #pulseaudio label, #network, #language, #custom-notification, #cpu, #memory, #temperature, #keyboard-state label, #backlight, #battery, #battery#bat2, #clock, #idle_inhibitor {
          color: ${custom.foreground};
          border-radius: 0.5em;
          padding: 0.5em;
          margin-left: 7px;
      }
      #clock {
          background: ${custom.alpha_background};
      }
      #window{
          background: ${custom.palette.tertiary_background_hex};
      }
    '';
  };
}
