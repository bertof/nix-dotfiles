{ pkgs, ... }:
let
  update_time = "10m";
in
{
  home.packages = [ pkgs.update-background ];

  systemd.user.services."update-background" = {
    Unit = {
      Description = "Set random desktop background using feh";
      After = [ "graphical-session.pre.target" ];
      PartOf = [ "graphical-session.target" ];
      RequiresMountsFor = [ "/home/bertof/Immagini" ];
    };
    Install = {
      WantedBy = [ "graphical-session.target" ];
    };
    Service = {
      Type = "oneshot";
      IOSchedulingClass = "idle";
      ExecStart = "${pkgs.update-background}/bin/update-background";
    };
  };

  systemd.user.timers."update-background" = {
    Unit = {
      Description = "Set random desktop background using feh";
    };
    Timer = {
      OnUnitActiveSec = update_time;
    };
    Install = {
      WantedBy = [ "timers.target" ];
    };
  };
}
