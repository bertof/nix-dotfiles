{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      hunspell
      libreoffice-fresh
      ;
    inherit (pkgs.hunspellDicts) en_GB-large en_US-large it_IT;
  };
}
