{ pkgs, ... }:
{
  programs.obs-studio = {
    enable = true;
    package = pkgs.obs-studio;
    plugins = builtins.attrValues {
      inherit (pkgs.obs-studio-plugins)
        # obs-backgroundremoval
        # obs-multi-rtmp
        # obs-nvfbc
        obs-pipewire-audio-capture
        obs-vkcapture
        obs-backgroundremoval
        input-overlay
        # obs-linuxbrowser
        # obs-gstreamer
        # obs-move-transition
        # obs-multi-rtmp
        ;
    };
  };
}
