{ pkgs, ... }: {
  home.packages = [ pkgs.wl-update-background ];

  systemd.user.services."wl-update-background" = {
    Unit = {
      Description = "Set random desktop background using swww";
      After = [ "graphical-session.pre.target" ];
      PartOf = [ "graphical-session.target" ];
      RequiresMountsFor = [ "/home/bertof/Immagini" ];
    };
    Install = {
      WantedBy = [ "graphical-session.target" ];
    };
    Service = {
      Type = "oneshot";
      IOSchedulingClass = "idle";
      ExecStart = "${pkgs.wl-update-background}/bin/wl-update-background";
    };
  };

  systemd.user.timers."wl-update-background" = {
    Unit = {
      Description = "Set random desktop background using swww";
    };
    Timer = {
      OnUnitActiveSec = "10m";
    };
    Install = {
      WantedBy = [ "timers.target" ];
    };
  };
}
