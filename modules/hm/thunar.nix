{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs.xfce)
      thunar
      thunar-archive-plugin
      thunar-media-tags-plugin
      thunar-volman
      tumbler
      xfconf
      ;
  };
}
