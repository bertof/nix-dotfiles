{ pkgs, ... }:
let
  systemLibFolder = "/run/current-system/sw/lib/";
  userLibFolder = "$HOME/.local/state/nix/profiles/home-manager/home-path/lib";
  variables = {
    DSSI_PATH = "$HOME/.dssi:${userLibFolder}/dssi:${systemLibFolder}/dssi";
    LADSPA_PATH = "$HOME/.ladspa:${userLibFolder}/ladspa:${systemLibFolder}/ladspa";
    LV2_PATH = "$HOME/.lv2:${userLibFolder}/lv2:${systemLibFolder}/lv2";
    LXVST_PATH = "$HOME/.lxvst:${userLibFolder}/lxvst:${systemLibFolder}/lxvst";
    VST_PATH = "$HOME/.vst:${userLibFolder}/vst:${systemLibFolder}/vst";
    VST3_PATH = "$HOME/.vst3:${userLibFolder}/vst3:${systemLibFolder}/vst3";
  };
in
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      # Plugins
      # helm
      # noise-repellent
      # speech-denoiser
      # stochas
      # sunvox
      # tunefish
      # zrythm
      # mamba
      aeolus
      ardour
      artyFX
      autotalent
      boops
      # breeze-icons
      calf
      cardinal
      # carla
      fluidsynth
      gxplugins-lv2
      geonkick
      giada
      hydrogen
      neural-amp-modeler-lv2
      proteus
      ninjas2
      lsp-plugins
      # mixxx
      rnnoise
      samplv1
      sfizz
      surge
      surge-XT
      talentedhack
      vital
      vocproc
      x42-plugins
      zam-plugins
      zita-at1
      zyn-fusion
      zynaddsubfx
      ;
  };

  home.sessionVariables = variables;
}
