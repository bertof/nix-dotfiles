{ pkgs, ... }:
{
  home.packages = builtins.attrValues { inherit (pkgs) nix-zsh-completions zsh-completions; };

  programs.zsh = {
    enable = true;
    autocd = true;
    syntaxHighlighting.enable = true;
    plugins = [ ];
    initExtraBeforeCompInit = ''
      zstyle ':completion:*' menu select
      setopt CORRECT
      setopt AUTO_CD
      setopt CHASE_LINKS
      setopt PUSHD_TO_HOME
    '';

    oh-my-zsh = {
      enable = true;
      plugins = [
        # "common-aliases"
        "cp"
        "dirhistory"
        "git-auto-fetch"
        "git"
        "sudo"
      ];
      extraConfig = "";
    };

    # prezto = {
    #   enable = true;
    # };

  };
}
