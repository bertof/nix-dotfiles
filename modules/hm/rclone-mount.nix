{ pkgs, lib, config, nixosConfig, ... }:
let

  rclone_config = nixosConfig.age.secrets."s3_${nixosConfig.networking.hostName}".path;
  mounts = { "minio" = "/home/${config.home.username}/minio/"; };
in
{
  systemd.user.services = lib.attrsets.mapAttrs'
    (name: path: {
      name = "rclone-${name}";
      value = {
        Unit = {
          Description = "rclone mount for ${name}";
          After = [ "default.target" ];
          PartOf = [ "default.target" ];
        };
        Install.WantedBy = [ "default.target" ];
        Service = {
          Type = "simple";
          ExecStartPre = "${pkgs.coreutils}/bin/mkdir -p ${path}";
          ExecStart = "${pkgs.rclone}/bin/rclone --config ${rclone_config} mount ${name}:/ ${path} -v --vfs-cache-mode=writes --s3-upload-concurrency 32 --s3-chunk-size 128000";
          Restart = "on-failure";
          RestartSec = "30s";
        };
      };
    })
    mounts;
}
