{
  programs.bat = {
    enable = true;
    config = {
      theme = "Nord";
      tabs = "2";
    };
  };
}
