{ pkgs, ... }:
let
  dfp = pkgs.dwarf-fortress-packages;
  package = dfp.dwarf-fortress-full.override {
    # theme = dfp.themes.ironhand;
    # theme = dfp.themes.vettlingr;
    enableIntro = false;
    enableFPS = true;
  };
in
{
  home.packages = [ package ];
}
