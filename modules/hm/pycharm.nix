{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs) python3;
    inherit (pkgs.jetbrains) pycharm-professional jdk;
  };
}
