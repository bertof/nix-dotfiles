{ pkgs, nixosConfig, ... }:
{
  gtk = {
    enable = true;
    font = nixosConfig.nix-rice.rice.font.normal;
    iconTheme = {
      package = pkgs.qogir-icon-theme;
      name = "Qogir-dark";
    };
    theme = {
      package = pkgs.arc-theme;
      name = "Arc-Dark";
    };
  };

  qt = {
    enable = true;
    platformTheme.name = "gtk";
  };

  home = {
    pointerCursor = {
      package = pkgs.qogir-icon-theme;
      name = "Qogir";
      # size = 64;
      # x11.enable = true;
      gtk.enable = true;
    };
  };
}
