{ pkgs, ... }:
let
  libinputGesturesCmd = "${pkgs.libinput-gestures}/bin/libinput-gestures";
  xdotool = "${pkgs.xdotool}/bin/xdotool";
in
{
  systemd.user.services."libinput-gestures" = {
    Unit = {
      Description = "Touchpad gestures";
      After = [ "graphical-session-pre.target" ];
      PartOf = [ "graphical-session.target" ];
    };
    Install = {
      WantedBy = [ "graphical-session.target" ];
    };
    Service = {
      Type = "simple";
      ExecStart = libinputGesturesCmd;
    };
  };

  xdg.configFile."libinput-gestures.conf".text = ''
    gesture swipe left ${xdotool} key alt+Right
    gesture swipe right ${xdotool} key alt+Left
    gesture swipe up ${xdotool} key alt+Tab
  '';
}
