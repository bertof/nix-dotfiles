{ nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  inherit (nixosConfig.nix-rice) rice;
  strPalette = palette.toRgbShortHex rice.colorPalette;
in
{
  programs.mangohud = {
    enable = true;
    # enableSessionWide = true;
    settings = {
      toggle_fps_limit = "F1";
      cpu_color = strPalette.normal.blue;
      cpu_temp = true;
      engine_color = strPalette.normal.red;
      gpu_color = strPalette.normal.green;
      gpu_stats = true;
      gpu_temp = true;
      io_color = strPalette.normal.white;
      media_player_color = strPalette.normal.white;
      media_player_name = "spotify";
      output_folder = "/home/bertof";
      position = "top-left";
      ram = true;
      ram_color = strPalette.normal.magenta;
      toggle_hud = "Shift_L+F12";
      toggle_logging = "Shift_L+F2";
      vram = true;
      vram_color = strPalette.dark.magenta;
    };
  };
}
