{
  programs.mpv = {
    enable = true;
    bindings = {
      "Ctrl+Right" = "playlist-next";
      "Ctrl+Left" = "playlist-prev";
    };
  };
}
