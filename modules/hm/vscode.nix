{ pkgs, ... }: {
  programs.vscode = {
    enable = true;
    extensions = with pkgs.vscode-extensions; [
      coder.coder-remote
      james-yu.latex-workshop
      # rust-lang.rust-analyzer
    ];
  };
}
