{ nixosConfig
, pkgs
, config
, ...
}:
let
  inherit (nixosConfig.networking) hostName;
in
{
  home.packages = [ pkgs.ntfy-sh ];
  xdg.configFile."ntfy/client.yml".source =
    config.lib.file.mkOutOfStoreSymlink
      nixosConfig.age.secrets."ntfy-${hostName}".path;
}
