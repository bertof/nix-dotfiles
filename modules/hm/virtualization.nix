{ pkgs, ... }:
{
  home.packages = [
    pkgs.remmina
    pkgs.virt-manager
    pkgs.virt-viewer
  ];
}
