{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      # mendeley # Reference manager
      # logseq
      # drawio
      obsidian
      # zettlr
      zotero
      ;
  };
}
