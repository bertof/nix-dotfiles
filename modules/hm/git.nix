{ pkgs, ... }:
let
  shellAliases = {
    "g" = "git";
    "ga" = "git add";
    "gaa" = "git add --all";
    "gau" = "git add --update";
    "gav" = "git add --verbose";
    "gb" = "git branch";
    "gba" = "git branch -a";
    "gbd" = "git branch -d";
    "gbD" = "git branch -D";
    "gbl" = "git blame -b -w";
    "gbnm" = "git branch --no-merged";
    "gbr" = "git branch --remote";
    "gbs" = "git bisect";
    "gbsb" = "git bisect bad";
    "gbsg" = "git bisect good";
    "gbsr" = "git bisect reset";
    "gbss" = "git bisect start";
    "gc" = "git commit -v";
    "gc!" = "git commit -v --amend";
    "gcn!" = "git commit -v --no-edit --amend";
    "gca" = "git commit -v -a";
    "gca!" = "git commit -v -a --amend";
    "gcan!" = "git commit -v -a --no-edit --amend";
    "gcans!" = "git commit -v -a -s --no-edit --amend";
    "gcas" = "git commit -a -s";
    "gcb" = "git checkout -b";
    "gcl" = "git clone --recurse-submodules";
    "gclean" = "git clean -id";
    "gpristine" = "git reset --hard && git clean -dffx";
    "gco" = "git checkout";
    "gcount" = "git shortlog -sn";
    "gcp" = "git cherry-pick";
    "gcpa" = "git cherry-pick --abort";
    "gcpc" = "git cherry-pick --continue";
    "gd" = "git diff";
    "gdtd" = "git difftool -g --dir-diff";
    "gdca" = "git diff --cached";
    "gdcw" = "git diff --cached --word-diff";
    "gdct" = "git describe --tags $(git rev-list --tags --max-count=1)";
    "gds" = "git diff --staged";
    "gdt" = "git diff-tree --no-commit-id --name-only -r";
    "gdw" = "git diff --word-diff";
    "gf" = "git fetch";
    "gfa" = "git fetch --all --prune";
    "gfg" = "git ls-files | grep";
    "gignored" = ''git ls-files -v \| grep"^[[ :lower: ]]"'';
    "glo" = "git log --oneline --decorate";
    "glols" = "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --stat";
    "glola" = "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --all";
    "glog" = "git log --oneline --decorate --graph";
    "gloga" = "git log --oneline --decorate --graph --all";
    "gm" = "git merge";
    "gma" = "git merge --abort";
    "gp" = "git push";
    "gpa" = "git push --all";
    "gpf!" = "git push --force-with-lease";
    "gpoat" = "git push origin --all && git push origin --tags";
    "gpu" = "git push upstream";
    "gpv" = "git push -v";
    "gr" = "git remote";
    "gra" = "git remote add";
    "grb" = "git rebase";
    "grba" = "git rebase --abort";
    "grbc" = "git rebase --continue";
    "grbi" = "git rebase -i";
    "grev" = "git revert";
    "grh" = "git reset";
    "grhh" = "git reset --hard";
    "grm" = "git rm";
    "grmc" = "git rm --cached";
    "grs" = "git restore";
    "grv" = "git remote -v";
    "gss" = "git status -s";
    "gst" = "git status";
    "gsta" = "git stash push";
    "gstas" = "git stash save";
    "gstaa" = "git stash apply";
    "gstc" = "git stash clear";
    "gstd" = "git stash drop";
    "gstl" = "git stash list";
    "gstp" = "git stash pop";
    "gsts" = "git stash show --text";
    "gup" = "git pull --rebase";
    "gupv" = "git pull --rebase -v";
  };
in
{
  programs = {
    git = {
      enable = true;
      package = pkgs.gitFull;
      userName = "Filippo Berto";
      userEmail = "berto.f@protonmail.com";
      # signing = {
      #   key = "berto.f@protonmail.com";
      #   signByDefault = true;
      # };
      extraConfig = {
        "add.interactive".useBuiltin = false;
        branch.sort = "-committerdate";
        column.ui = "auto";
        commit.gpgSign = true;
        core = {
          editor = "hx";
          fsmonitor = true;
        };
        credential.helper = "libsecret";
        delta = {
          line-numbers = true;
          navigate = true;
          side-by-side = true;
          wrap-max-lines = "unlimited";
        };
        diff = {
          algorithm = "histogram";
          colorMoved = "default";
          guitool = "meld";
          tool = "meld";
        };
        init.defaultBranch = "main";
        merge = {
          conflictstyle = "diff3";
          guitool = "meld";
          tool = "meld";
        };
        pull.rebase = true;
        rebase = {
          autoStash = true;
          autoSquash = true;
        };
        rerere = {
          enabled = true;
          autoUpdate = true;
        };
        tag.sort = "version:refname";
        user.signingKey = "berto.f@protonmail.com";
      };
      lfs.enable = true;
      delta.enable = true;
    };
    bash.shellAliases = shellAliases;
    zsh.shellAliases = shellAliases;
  };
  home.packages = builtins.attrValues {
    inherit (pkgs)
      gh
      git-secret
      glab
      meld
      ;
  };
}
