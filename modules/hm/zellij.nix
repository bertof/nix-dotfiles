{ nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  strPalette = palette.toRgbHex nixosConfig.nix-rice.rice.colorPalette;
in
{
  programs.zellij = {
    enable = true;
    enableZshIntegration = false;
    enableBashIntegration = false;
    enableFishIntegration = false;
  };

  home.shellAliases = {
    "ze" = "zellij";
    "zec" = "zellij -l compact";
  };

  xdg.configFile."zellij/config.kdl".text = ''
    theme "nix-rice"
    pane_frames false
  '';

  xdg.configFile."zellij/themes/nix-rice.kdl".text = ''
    themes {
      nix-rice {
        bg "${strPalette.primary.dim_foreground}" // Foreground darker
        fg "${strPalette.primary.bright_foreground}" // Selection
        red "${strPalette.normal.red}"
        black "${strPalette.primary.background}" // Background
        green "${strPalette.normal.green}"
        yellow "${strPalette.normal.yellow}"
        blue "${strPalette.normal.blue}"
        magenta "${strPalette.normal.magenta}"
        cyan "${strPalette.normal.cyan}"
        white "${strPalette.primary.foreground}" // Foreground
        orange "${strPalette.bright.red}"
      }
    }
  '';
}
