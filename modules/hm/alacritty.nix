{ nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  inherit (nixosConfig.nix-rice) rice;
  strPalette = palette.toRgbHex rec {
    inherit (rice.colorPalette) normal bright primary;
    dim = rice.colorPalette.dark;
    cursor = {
      cursor = normal.white;
      text = normal.black;
    };
    vi_mode_cursor = {
      cursor = normal.white;
      text = normal.black;
    };
    selection.background = dim.blue;
    search = {
      matches.background = dim.cyan;
      bar = {
        foreground = dim.cyan;
        background = dim.yellow;
      };
    };
  };
in
{
  # Include fonts packages
  home.packages = [ rice.font.monospace.package ];
  programs.alacritty = {
    enable = true;
    settings = {
      key_bindings = [
        {
          key = "Return";
          mods = "Control|Shift";
          action = "SpawnNewInstance";
        }
      ];
      # env.TERM = "xterm-256color";
      env = {
        TERM = "alacritty";
        WINIT_X11_SCALE_FACTOR = "1";
      };
      scrolling.history = 3000;
      font = {
        normal.family = rice.font.monospace.name;
        inherit (rice.font.monospace) size;
      };
      window.opacity = rice.opacity;
      mouse = {
        # hide_when_typing = true;
        hints.modifiers = "Control";
      };
      colors = strPalette // {
        selection.text = "CellForeground";
        search.matches.foreground = "CellForeground";
      };
    };
  };
}
