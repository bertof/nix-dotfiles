{
  programs.ssh = {
    enable = true;
    compression = true;
    matchBlocks = {
      "*" = {
        identityFile = "~/.ssh/id_ed25519";
      };
    };
    extraConfig = ''
      # --- START CODER VSCODE coder.ricerca.sesar.di.unimi.it ---
      Host coder-vscode.coder.ricerca.sesar.di.unimi.it--*
        ConnectTimeout 0
        LogLevel ERROR
        ProxyCommand "/home/bertof/.config/Code/User/globalStorage/coder.coder-remote/coder.ricerca.sesar.di.unimi.it/bin/coder-linux-amd64" vscodessh --network-info-dir "/home/bertof/.config/Code/User/globalStorage/coder.coder-remote/net" --session-token-file "/home/bertof/.config/Code/User/globalStorage/coder.coder-remote/coder.ricerca.sesar.di.unimi.it/session_token" --url-file "/home/bertof/.config/Code/User/globalStorage/coder.coder-remote/coder.ricerca.sesar.di.unimi.it/url" %h
        SetEnv  CODER_SSH_SESSION_TYPE=vscode
        StrictHostKeyChecking no
        UserKnownHostsFile /dev/null
      # --- END CODER VSCODE coder.ricerca.sesar.di.unimi.it ---
    '';
  };
}
