{ nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  inherit (nixosConfig.nix-rice) rice;
  strPalette = palette.toRGBHex rice.colorPalette;
  font = rice.font.normal;
in
{
  services.twmn = {
    enable = true;
    text = {
      color = strPalette.primary.foreground;
      font = {
        inherit (font) package size;
        family = font.name;
      };
    };
    window = {
      animation.bounce.enable = false;
      color = strPalette.primary.background;
      height = 32;
      opacity = float.round (rice.opacity * 100);
      offset = {
        x = -20;
        y = 50;
      };
    };
  };
}
