{
  programs.keychain = {
    enable = true;
    keys = [ "id_ed25519" ];
    agents = [
      "gpg"
      "ssh"
    ];
    enableBashIntegration = true;
    enableZshIntegration = true;
    enableXsessionIntegration = true;
  };
}
