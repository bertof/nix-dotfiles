{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      nmap# RECOGNITION

      # WEB
      httpie
      # altair
      # burpsuite

      bintools
      ghidra-bin
      radare2# REVERSING
      # cutter

      # INFRASTRUCTURE
      # cocktail-bar-cli
      ;
  };
}
