{ pkgs, ... }:

let
  gnomeExtensions = builtins.attrValues {
    inherit (pkgs.gnomeExtensions)
      appindicator
      aylurs-widgets
      babar
      caffeine
      dash-to-dock
      dash-to-panel
      # dash-to-plank
      # devbar
      dock-from-dash
      # espresso
      # focus
      freon
      gamemode
      # git
      gtile
      # jiggle
      just-perfection
      # keyman
      spotify-tray
      topiconsfix
      # tray-icons
      tray-icons-reloaded
      unite
      user-themes
      # vitals
      ;
  };
in
{
  home.packages = [ pkgs.gnome.gnome-tweaks ] ++ gnomeExtensions;
}
