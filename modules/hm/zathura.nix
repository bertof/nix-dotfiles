{ nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  inherit (nixosConfig.nix-rice) rice;
  strPalette = palette.toRgbHex rice.colorPalette;
in
{
  programs.zathura = {
    enable = true;
    mappings = {
      "<C-i>" = "recolor";
    };
    options = {
      # completion-bg = strPalette.bright.black;
      # default-bg = strPalette.normal.black;
      font = "${rice.font.normal.name} 10";
      # inputbar-bg = strPalette.bright.black;
      # inputbar-fg = strPalette.normal.cyan;
      page-padding = 10;
      # recolor = true;
      # recolor-lightcolor = strPalette.primary.background;
      # recolor-darkcolor = strPalette.primary.foreground;
      # statusbar-bg = strPalette.bright.black;
      selection-clipboard = "clipboard";
      filemonitor = "signal";

      # adapted from https://github.com/eastack/zathura-gruvbox
      notification-error-bg = strPalette.primary.background;
      notification-error-fg = strPalette.bright.red;
      notification-warning-bg = strPalette.primary.background;
      notification-warning-fg = strPalette.bright.yellow;
      notification-bg = strPalette.primary.background;
      notification-fg = strPalette.bright.green;
      completion-bg = strPalette.bright.black;
      completion-fg = strPalette.bright.white;
      completion-group-bg = strPalette.dark.black;
      completion-group-fg = strPalette.dark.white;
      completion-highlight-bg = strPalette.bright.cyan;
      completion-highlight-fg = strPalette.bright.black;

      # Define the color in index mode
      index-bg = strPalette.normal.black;
      index-fg = strPalette.normal.white;
      index-active-bg = strPalette.normal.cyan;
      index-active-fg = strPalette.normal.black;
      inputbar-bg = strPalette.primary.background;
      inputbar-fg = strPalette.bright.white;
      statusbar-bg = strPalette.bright.black;
      statusbar-fg = strPalette.bright.white;
      highlight-color = strPalette.bright.yellow;
      highlight-active-color = strPalette.bright.red;
      default-bg = strPalette.primary.background;
      default-fg = strPalette.bright.white;
      render-loading = true;
      render-loading-bg = strPalette.primary.background;
      render-loading-fg = strPalette.bright.white;

      # Recolor book content's color
      recolor-lightcolor = strPalette.primary.background;
      recolor-darkcolor = strPalette.bright.white;
      # recolor = true;
      recolor-keephue = true; # keep original color
    };
  };
}
