{
  services.grobi = {
    enable = true;
    rules = [
      {
        name = "Nvidia2screens";
        outputs_connected = [
          "HDMI-0"
          "eDP-1-1"
        ];
        configure_column = [
          "HDMI-0"
          "eDP-1-1"
        ];
        primary = "HDMI-0";
      }
      {
        name = "FallbackIntel";
        outputs_connected = [ "eDP1" ];
        configure_single = "eDP1";
      }
      {
        name = "FallbackNvidia";
        outputs_connected = [ "eDP-1-1" ];
        configure_single = "eDP-1-1";
      }
    ];
  };
}
