{ pkgs
, lib
, config
, ...
}:
{
  programs.go = {
    enable = true;
    goPath = ".go";
  };

  home.packages =
    (lib.optionals config.programs.helix.enable [ pkgs.gopls ])
    ++ (lib.optionals config.programs.neovim.enable [ pkgs.gopls ])
    ++ (lib.optionals config.programs.kakoune.enable [ pkgs.gopls ]);
}
