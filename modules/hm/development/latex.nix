{ pkgs
, config
, lib
, ...
}:
{
  home.packages = [
    # pkgs.bibtool
    # pkgs.texlive.combined.scheme-full
    pkgs.zathura
  ]
  ++ lib.optionals config.programs.helix.enable [
    # pkgs.texlab
  ]
  ++ lib.optionals config.programs.kakoune.enable [
    # pkgs.texlab
    # pkgs.aspellDicts.en
    # pkgs.aspellDicts.en-computers
    # pkgs.aspellDicts.en-science
    # pkgs.aspellDicts.it
  ];

  home.file.".latexmkrc".text = ''
    $pdf_previewer = 'zathura %O %S';
    $pdf_update_method = 2
  '';
}
