{ pkgs
, lib
, config
, ...
}:
{
  home.packages =
    lib.optionals config.programs.helix.enable
      (
        builtins.attrValues { inherit (pkgs.nodePackages) typescript-language-server; }
      )
    ++ lib.optionals config.programs.kakoune.enable (
      builtins.attrValues { inherit (pkgs.nodePackages) typescript-language-server; }
    );
  programs.neovim.withNodeJs = true;
}
