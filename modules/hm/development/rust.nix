{ pkgs
, lib
, config
, ...
}:
let
  tomlGenerate = (pkgs.formats.toml { }).generate;
in
{
  home.packages =
    (builtins.attrValues {
      inherit (pkgs)
        bacon
        cargo
        cargo-audit
        cargo-criterion
        cargo-deadlinks
        cargo-expand
        cargo-flamegraph
        cargo-fuzz
        cargo-hack
        cargo-hakari
        cargo-modules
        cargo-outdated
        cargo-profiler
        cargo-release
        cargo-show-asm
        cargo-spellcheck
        cargo-tarpaulin
        cargo-udeps
        cargo-watch
        cargo-workspaces
        clippy
        critcmp
        rustc
        rustfmt
        ;
      # cargo-about
      # cargo-auditable
      # cargo-deny
      # cargo-deps
      # cargo-feature
    })
    ++ lib.optionals config.programs.helix.enable (
      builtins.attrValues {
        inherit (pkgs)
          # lldb
          rust-analyzer;
      }
    )
    ++ lib.optionals config.programs.kakoune.enable (
      builtins.attrValues { inherit (pkgs) rust-analyzer; }
    );

  home.file.".cargo/config.toml".source = tomlGenerate "cargo-config" {
    build.rustc-wrapper = "${pkgs.sccache}/bin/sccache";
  };

  xdg.configFile."cargo-release/release.toml".source = tomlGenerate "release.toml" {
    sign-commit = true;
    sign-tag = true;
    publish = false;
  };
}
