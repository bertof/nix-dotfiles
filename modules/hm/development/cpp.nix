{ pkgs
, config
, lib
, ...
}:
{
  home.packages =
    lib.optionals config.programs.helix.enable
      (
        builtins.attrValues { inherit (pkgs) clang-tools cmake-language-server; }
      )
    ++ lib.optionals config.programs.kakoune.enable (
      builtins.attrValues { inherit (pkgs) clang-tools cmake-language-server; }
    );
}
