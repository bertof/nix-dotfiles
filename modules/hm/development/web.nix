{ pkgs, ... }:
{
  home.packages = [
    pkgs.bruno
    pkgs.httpie
  ];
}
