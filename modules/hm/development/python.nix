{ pkgs
, lib
, config
, ...
}:

let
  py = pkgs.python3;
  pyPkgs = py.pkgs;
in
{
  home.packages =
    [
      py
      pyPkgs.black
    ]
    ++ lib.optionals config.programs.helix.enable (
      (builtins.attrValues { inherit (pyPkgs) python-lsp-server pyls-flake8 pyls-isort; })
      ++ pyPkgs.python-lsp-server.optional-dependencies.all
    )
    ++ lib.optionals config.programs.kakoune.enable (
      (builtins.attrValues { inherit (pyPkgs) python-lsp-server pyls-flake8 pyls-isort; })
      ++ pyPkgs.python-lsp-server.optional-dependencies.all
    );
  programs.neovim.withPython3 = true;
}
