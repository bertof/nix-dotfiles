{ pkgs
, config
, lib
, ...
}:
{
  home.packages =
    # (builtins.attrValues { inherit (pkgs) docker-compose; })
    # ++
    lib.optionals config.programs.helix.enable (
      builtins.attrValues {
        inherit (pkgs.nodePackages) dockerfile-language-server-nodejs;
        inherit (pkgs) docker-compose-language-service;
      }
    );
  # home.shellAliases = {
  #   "dkcd" = "docker-compose down";
  #   "dkc" = "docker-compose";
  #   "dkcu" = "docker-compose up";
  #   "dk" = "docker";
  # };
}
