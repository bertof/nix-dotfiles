{ pkgs, ... }:
let
  helm = pkgs.wrapHelm pkgs.kubernetes-helm {
    plugins = builtins.attrValues {
      inherit (pkgs.kubernetes-helmPlugins)
        helm-diff
        # helm-secrets
        ;
    };
  };
in
{
  home = {
    shellAliases = {
      "k" = "kubectl";
    };
    packages = [
      # pkgs.kustomize
      # pkgs.lens
      helm
      pkgs.awscli2
      pkgs.k9s
      pkgs.kubectl
      pkgs.kubelogin-oidc
      pkgs.kubevirt
    ];
  };
}
