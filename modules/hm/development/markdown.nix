{ pkgs
, lib
, config
, ...
}:
{
  home.packages =
    lib.optionals config.programs.helix.enable [ pkgs.marksman ];
}
