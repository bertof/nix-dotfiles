{ pkgs
, config
, lib
, ...
}:
{
  home.packages =
    (builtins.attrValues {
      inherit (pkgs)
        # nixpkgs-review
        nix-prefetch-scripts
        nix-tree
        nixfmt-classic
        nixpkgs-fmt
        ;
    })
    ++ lib.optionals config.programs.helix.enable [ pkgs.nil ]
    ++ lib.optionals config.programs.kakoune.enable [ pkgs.rnix-lsp ];
  programs.neovim.plugins = [ pkgs.vimPlugins.vim-nix ];
}
