{ pkgs, ... }:
{
  home.packages = builtins.attrValues { inherit (pkgs.jetbrains) datagrip jdk; };
}
