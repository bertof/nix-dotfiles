{ config
, pkgs
, lib
, ...
}:
{
  home.packages =
    lib.optionals config.programs.helix.enable
      (
        builtins.attrValues {
          inherit (pkgs) yaml-language-server taplo-cli;
          inherit (pkgs.nodePackages) vscode-langservers-extracted;
        }
      )
    ++ lib.optionals config.programs.kakoune.enable (
      builtins.attrValues {
        inherit (pkgs) yaml-language-server;
        inherit (pkgs.nodePackages) vscode-langservers-extracted;
      }
    );
}
