{ pkgs
, config
, lib
, ...
}:
{
  home.packages =
    [ pkgs.bitwarden ]
    ++ lib.optionals config.programs.rofi.enable (
      builtins.attrValues {
        # inherit (pkgs.unstable_pkgs) rbw rofi-rbw pinentry-gtk2;
      }
    );
}
