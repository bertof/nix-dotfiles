{ nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  inherit (nixosConfig.nix-rice) rice;
  strPalette = palette.toRgbHex {
    inherit (rice.colorPalette)
      background
      foreground
      color0
      color1
      color2
      color3
      color4
      color5
      color6
      color7
      color8
      color9
      color10
      color11
      color12
      color13
      color14
      color15
      ;
  };
in
{
  programs.kitty = {
    enable = true;
    settings = {
      scrollback_lines = 10000;
      enable_audio_bell = false;
      visual_bell_duration = toString 0.1;
      update_check_interval = 0;
      background_opacity = toString rice.opacity;
      close_on_child_death = "yes";
      clipboard_control = "write-clipboard write-primary read-clipboard read-primary";
      disable_ligatures = "never";
      editor = "nvim";
    } // strPalette;
    keybindings = {
      "ctrl+shift+t" = "new_tab_with_cwd";
      "ctrl+shift+enter" = "new_window_with_cwd";
      "ctrl+shift+n" = "new_os_window_with_cwd";
      "ctrl+shift+up" = "previous_window";
      "ctrl+shift+down" = "next_window";
    };
    font = rice.font.monospace;
  };
}
