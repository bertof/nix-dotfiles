{
  editorconfig = {
    enable = true;
    settings = {
      "*" = {
        charset = "utf-8";
        end_of_line = "lf";
        indent_size = "2";
        # max_line_width = "120";
        indent_style = "space";
        trim_trailing_whitespace = "true";
        insert_final_newline = "true";
      };
      "*.py" = {
        indent_size = 4;
      };
    };
  };
}
