{ pkgs, lib, ... }:
# Categories: https://specifications.freedesktop.org/menu-spec/latest/apa.html#main-category-registry
let
  # chrome-cmd = link: "${pkgs.google-chrome}/bin/google-chrome-stable --app=${link}";
  # vivaldi-cmd = link: "${pkgs.vivaldi}/bin/vivaldi --app=${link}";
  # firefox-cmd = link: "firefox --new-window ${link}";
  open-cmd = link: "${pkgs.xdg-utils}/bin/xdg-open ${link}";
  webAppBuilder =
    { desktopName
    , name ? builtins.replaceStrings [ " " ] [ "_" ] desktopName
    , link
    , cmd ? open-cmd
    , icon ? lib.toLower name
    , comment ? null
    , categories ? [ "Network" ]
    ,
    }:
    pkgs.makeDesktopItem {
      inherit
        name
        icon
        desktopName
        comment
        categories
        ;
      exec = cmd link;
    };
  links = [
    {
      link = "https://chess.com";
      desktopName = "Chess.com";
      categories = [ "Game" ];
    }
    {
      link = "https://google.com/calendar";
      desktopName = "Google Calendar";
      categories = [ "Office" ];
    }
    {
      link = "https://keep.google.com";
      desktopName = "Google Keep";
      categories = [ "Office" ];
    }
    {
      link = "https://play.google.com/books";
      desktopName = "Google Play Books";
      categories = [
        "Office"
        "Viewer"
      ];
    }
    # {
    #   link = "https://teams.microsoft.com";
    #   desktopName = "Microsoft Teams";
    #   categories = [ "Office" ];
    #   cmd = chrome-cmd;
    # }
    {
      link = "https://monkeytype.com/";
      desktopName = "Monkeytype";
      categories = [ "Game" ];
    }
    {
      link = "https://www.netflix.com";
      desktopName = "Netflix";
      categories = [ "AudioVideo" ];
    }
    {
      link = "https://notion.so";
      desktopName = "Notion";
      categories = [ "Office" ];
    }
    {
      link = "https://app.plex.tv";
      desktopName = "Plex";
      categories = [ "AudioVideo" ];
    }
    {
      link = "https://primevideo.com";
      desktopName = "Prime Video";
      categories = [ "AudioVideo" ];
    }
    {
      link = "https://mail.protonmail.com";
      desktopName = "Protonmail";
      categories = [ "Office" ];
    }
    {
      link = "https://www.twitch.tv";
      desktopName = "Twitch";
      categories = [ "AudioVideo" ];
    }
    {
      link = "https://web.whatsapp.com/";
      desktopName = "WhatsApp";
    }
    {
      link = "https://www.youtube.com";
      desktopName = "YouTube";
      categories = [ "AudioVideo" ];
    }
  ];

in
{
  home.packages = map webAppBuilder links;
}
