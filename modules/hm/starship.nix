{
  programs.starship = {
    enable = true;
    enableBashIntegration = true;
    enableNushellIntegration = true;
    enableZshIntegration = true;
    settings = {
      command_timeout = 1000;
      directory.truncation_symbol = "…/";
      hostname.format = "[$hostname]($style) "; # Disable ssh symbol
      # line_break.disabled = true; # Prompt in one line
      nix_shell.symbol = "❄️ "; # better soacing
      python.python_binary = [
        "python3"
        "python2"
      ];
      status = {
        disabled = false; # enable module
        format = "[$symbol $status]($style) "; # nicer status format
      };
      username.format = "[$user]($style)@"; # compact username format
    };
  };
}
