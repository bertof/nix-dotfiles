{ nixosConfig, ... }:
{
  services.spotifyd = {
    enable = true;
    settings.global = {
      username = "filippoberto95";
      password_cmd = "cat ${nixosConfig.age.secrets.spotify_password.path}";
      backend = "pulseaudio";
    };
  };
}
