{ pkgs, ... }:

let
  thumbnailer = "${pkgs.libheif.bin}/bin/heif-thumbnailer";
in
{
  # home.packages = [
  #   pkgs.libheif.bin
  # ];

  xdg.dataFile."thumbnailers/heic.thumbnailer".text = ''
    [Thumbnailer Entry]
    TryExec=${thumbnailer}
    Exec=${thumbnailer} -s %s %i %o
    MimeType=image/heif;image/heic
  '';
}
