{ pkgs, ... }:
{
  home = {
    sessionVariables = {
      EDITOR = "hx";
      VISUAL = "hx";
    };
    shellAliases = {
      x = "hx";
    };
    packages = builtins.attrValues {
      inherit (pkgs) nil; # Nix language server
      # inherit desktopItem;
    };
  };
  programs.helix = {
    enable = true;
    languages.language = [
      {
        name = "nix";
        auto-format = true;
      }
      {
        name = "typst";
        auto-format = true;
        formatter.command = "typstyle";
      }
    ];
    languages.language-server = {
      nil.config.nil = {
        formatting.command = [ "nixpkgs-fmt" ];
        autoEvalInputs = true;
      };
      texlab.config.texlab = {
        formatterLineLength = 0;
        bibtexFormatter = "latexindent";
        latexindent.modifyLineBreaks = false;
        # chktex.onOpenAndSave = true;
        chktex.onEdit = true;
      };
      yaml-language-server.config.yaml = {
        keyOrdering = false;
        schemas = {
          "https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json" = "*gitlab-ci*.{yml,yaml}";
          "https://json.schemastore.org/ansible-playbook" = "*play*.{yml,yaml}";
          "https://json.schemastore.org/ansible-stable-2.9" = "roles/tasks/*.{yml,yaml}";
          "https://json.schemastore.org/chart" = "Chart.{yml,yaml}";
          "https://json.schemastore.org/dependabot-v2" = ".github/dependabot.{yml,yaml}";
          "https://json.schemastore.org/github-action" = ".github/action.{yml,yaml}";
          "https://json.schemastore.org/github-workflow" = ".github/workflows/*";
          "https://json.schemastore.org/hugo" = "hugo.yaml";
          "https://json.schemastore.org/kustomization" = "kustomization.{yml,yaml}";
          "https://json.schemastore.org/prettierrc" = ".prettierrc.{yml,yaml}";
          "https://raw.githubusercontent.com/OAI/OpenAPI-Specification/main/schemas/v3.1/schema.json" = "*api*.{yml,yaml}";
          "https://raw.githubusercontent.com/argoproj/argo-workflows/master/api/jsonschema/schema.json" = "*flow*.{yml,yaml}";
          "https://raw.githubusercontent.com/compose-spec/compose-spec/master/schema/compose-spec.json" = "*docker-compose*.{yml,yaml}";
          kubernetes = "*.yaml";
        };
      };
    };
    settings = {
      theme = "nightfox";
      editor = {
        lsp.display-messages = true;
        lsp.display-inlay-hints = true;
        scrolloff = 5;
        soft-wrap.enable = true;
        true-color = true;
        indent-guides = {
          render = true;
          character = "▏";
        };
      };
      keys.normal = {
        "C-A-l" = ":format";
        "A-r" = ":reload";
        "C-A-r" = ":reload-all";
      };
    };
    themes =
      let
        transparent = "none";
        gray = "#665c54";
        dark-gray = "#3c3836";
        white = "#fbf1c7";
        black = "#282828";
        red = "#fb4934";
        green = "#b8bb26";
        yellow = "#fabd2f";
        orange = "#fe8019";
        blue = "#83a598";
        magenta = "#d3869b";
        cyan = "#8ec07c";
      in
      {
        base16 = {
          "ui.menu" = transparent;
          "ui.menu.selected" = {
            modifiers = [ "reversed" ];
          };
          "ui.linenr" = {
            fg = gray;
            bg = dark-gray;
          };
          "ui.popup" = {
            modifiers = [ "reversed" ];
          };
          "ui.linenr.selected" = {
            fg = white;
            bg = black;
            modifiers = [ "bold" ];
          };
          "ui.selection" = {
            fg = black;
            bg = blue;
          };
          "ui.selection.primary" = {
            modifiers = [ "reversed" ];
          };
          "comment" = {
            fg = gray;
          };
          "ui.statusline" = {
            fg = white;
            bg = dark-gray;
          };
          "ui.statusline.inactive" = {
            fg = dark-gray;
            bg = white;
          };
          "ui.help" = {
            fg = dark-gray;
            bg = white;
          };
          "ui.cursor" = {
            modifiers = [ "reversed" ];
          };
          "variable" = red;
          "variable.builtin" = orange;
          "constant.numeric" = orange;
          "constant" = orange;
          "attributes" = yellow;
          "type" = yellow;
          "ui.cursor.match" = {
            fg = yellow;
            modifiers = [ "underlined" ];
          };
          "string" = green;
          "variable.other.member" = red;
          "constant.character.escape" = cyan;
          "function" = blue;
          "constructor" = blue;
          "special" = blue;
          "keyword" = magenta;
          "label" = magenta;
          "namespace" = blue;
          "diff.plus" = green;
          "diff.delta" = yellow;
          "diff.minus" = red;
          "diagnostic" = {
            modifiers = [ "underlined" ];
          };
          "ui.gutter" = {
            bg = black;
          };
          "info" = blue;
          "hint" = dark-gray;
          "debug" = dark-gray;
          "warning" = yellow;
          "error" = red;
        };
      };
  };
}
