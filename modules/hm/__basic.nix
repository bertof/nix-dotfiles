{ pkgs, ... }:
{
  home = {
    language.base = "it_IT.UTF-8";
    packages = builtins.attrValues {
      inherit (pkgs)
        fastfetch
        fd
        file
        htop
        mmv-go
        nixos-option
        pv
        ripgrep
        unrar
        unzip
        wget
        xclip
        yq
        zip
        ;
    };
  };

  imports = [
    ./configurations.nix

    ./bash.nix
    ./bat.nix
    # ./bottom.nix
    # ./broot.nix
    ./dircolors.nix
    ./direnv.nix
    # ./exa.nix
    ./lsd.nix
    ./fzf.nix
    ./git.nix
    ./gpg.nix
    ./info.nix
    ./jq.nix
    # ./joshuto.nix
    ./keychain.nix
    # ./lf.nix
    ./man.nix
    # ./ntfy.nix
    ./shell_aliases.nix
    ./ssh.nix
    ./starship.nix
    ./tmux.nix
    ./yazi.nix
    ./zellij.nix
    ./zoxide.nix
    ./zsh.nix
  ];
}
