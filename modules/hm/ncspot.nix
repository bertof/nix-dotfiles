{
  programs.ncspot = {
    enable = true;
    # package = pkgs.ncspot.overrideAttrs (o: {
    #   buildFeatures = o.buildFeatures ++ [ "cover" "mpris" "notify" "share_clipboard" ];
    # });
    settings = {
      use_nerdfont = true;
      notify = true;
    };
  };
}
