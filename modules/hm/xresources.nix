{ pkgs, nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  inherit (nixosConfig.nix-rice) rice;
  strPalette = palette.toRgbHex rice.colorPalette;
in
{
  home.file.".Xresources" = {
    text = ''
      Xcursor.theme: "Quogir"
      Xcursor.size: 32

      *.foreground: ${strPalette.primary.foreground}
      *.background: ${strPalette.primary.background}
      *.cursorColor: ${strPalette.normal.white}
      *.color0:     ${strPalette.normal.black}
      *.color1:     ${strPalette.normal.red}
      *.color2:     ${strPalette.normal.green}
      *.color3:     ${strPalette.normal.yellow}
      *.color4:     ${strPalette.normal.blue}
      *.color5:     ${strPalette.normal.magenta}
      *.color6:     ${strPalette.normal.cyan}
      *.color7:     ${strPalette.normal.white}
      *.color8:     ${strPalette.bright.black}
      *.color9:     ${strPalette.bright.red}
      *.color10:    ${strPalette.bright.green}
      *.color11:    ${strPalette.bright.yellow}
      *.color12:    ${strPalette.bright.blue}
      *.color13:    ${strPalette.bright.magenta}
      *.color14:    ${strPalette.bright.cyan}
      *.color15:    ${strPalette.bright.white}

      xterm*faceName: ${rice.font.monospace.name}
      xterm*faceSize: ${toString rice.font.monospace.size}
    '';
    onChange = "${pkgs.xorg.xrdb}/bin/xrdb -merge ~/.Xresources ";
  };
}
