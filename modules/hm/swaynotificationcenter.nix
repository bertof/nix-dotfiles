{ pkgs, ... }:
{
  systemd.user.services.swaynotificationcenter = {
    Unit = {
      After = [ "graphical-session-pre.target" ];
      # Description = pkgs.swaynotificationcenter.meta.description;
      PartOf = [ "graphical-session.target" ];
    };
    Install.WantedBy = [ "graphical-session.target" ];
    Service = {
      ExecStart = "${pkgs.swaynotificationcenter}/bin/swaync";
      # ExecRestart = "${pkgs.swaynotificationcenter}/bin/swaync-client -R";
      # KillMode = "mixed";
      # Restart = "on-failure";
      Restart = "on-failure";
      RestartSec = "3s";
    };
  };

  xdg.configFile."swaync/config.json".text = ''
    {
      "$schema": "/etc/xdg/swaync/configSchema.json",
      "positionX": "right",
      "positionY": "top",
      "control-center-margin-top": 16,
      "control-center-margin-bottom": 16,
      "control-center-margin-right": 16,
      "notification-icon-size": 64,
      "notification-body-image-height": 100,
      "notification-body-image-width": 200,
      "timeout": 10,
      "timeout-low": 5,
      "timeout-critical": 0,
      "fit-to-screen": true,
      "control-center-width": 500,
      "control-center-height": 1000,
      "notification-window-width": 500,
      "keyboard-shortcuts": true,
      "image-visibility": "when-available",
      "transition-time": 200,
      "hide-on-clear": false,
      "hide-on-action": true,
      "script-fail-notify": true,
      "widgets": ["buttons-grid", "mpris", "volume", "title", "notifications"],
      "widget-config": {
        "title": {
          "text": "Notifications",
          "clear-all-button": true,
          "button-text": "󰆴  Clear"
        },
        "dnd": {
          "text": "Do Not Disturb"
        },
        "label": {
          "max-lines": 1,
          "text": "Notification Center"
        },
        "mpris": {
          "image-size": 96,
          "image-radius": 7
        },
        "volume": {
          "label": "󰕾"
        },
        "backlight": {
          "label": "󰃟"
        },
        "buttons-grid": {
          "actions": [
            {
              "label": "󰐥",
              "command": "wlogout"
            }
          ]
        }
      }
    }
  '';
}
