{ pkgs, ... }:
{
  programs.password-store = {
    package = pkgs.pass.withExtensions (
      ext:
      (builtins.attrValues {
        inherit (ext)
          # pass-audit
          pass-checkup
          pass-import
          pass-otp
          pass-tomb
          pass-update
          ;
      })
    );
    enable = true;
  };
}
