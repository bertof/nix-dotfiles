{ nixosConfig, config, lib, pkgs, ... }:
with nixosConfig.nix-rice.lib;
let
  strPalette = palette.toRgbaShortHex nixosConfig.nix-rice.rice.colorPalette;
  inherit (nixosConfig.networking) hostName;
in
{
  imports = [
    ./kitty.nix
    # ./nautilus.nix
    ./nemo.nix
    ./waybar.nix
    # ./dunst.nix
    # ./wlsunset.nix
    ./hyprlock.nix
    # ./swayidle.nix
    ./swaynotificationcenter.nix
    ./wl_update_background.nix
    ./hyprland-bluetooth-keyboard.nix
  ];

  home.packages = builtins.attrValues {
    inherit (pkgs)
      blueman
      brightnessctl
      gamescope
      grimblast
      networkmanagerapplet
      wireplumber
      playerctl
      sirula
      swaylock
      swww
      # walker
      wl-lockscreen
      waybar
      wl-clipboard
      wl-clipedit
      wlogout
      wofi
      wofi-emoji
      wtype
      ;
  };

  services = {
    network-manager-applet.enable = true;
    blueman-applet.enable = true;

    hypridle = {
      enable = true;
      settings = {
        general = {
          ignore_dbus_inhibit = false;
          lock_cmd = "pidof hyprlock || hyprlock"; # avoid starting multiple hyprlock instances.
          before_sleep_cmd = "loginctl lock-session"; # lock before suspend.
          after_sleep_cmd = "hyprctl dispatch dpms on"; # to avoid having to press a key twice to turn on the display
        };
        listener = [
          {
            timeout = 30;
            on-timeout = "brightnessctl -s -d intel_backlight s 10; brightnessctl -s -d tpacpi::kbd_backlight s 0"; # lower screen backlight
            on-resume = "brightnessctl -r -d intel_backlight; brightnessctl -r -d tpacpi::kbd_backlight"; # restore screen backlight
          }
          {
            timeout = 120;
            on-timeout = "loginctl lock-session"; # lock screen
          }
          {
            timeout = 300;
            on-timeout = "hyprctl dispatch dpms off"; # screen off when timeout has passed
            on-resume = "hyprctl dispatch dpms on;" + (lib.optionalString (hostName == "thor") "xrandr --output DP-1 --primary"); # screen on when activity is detected after timeout has fired.
          }
        ];
      };
    };
    hyprpaper = {
      enable = true;
      settings = {
        preload = "${../../wallpapers/n8ca4obsys991.png}";
        wallpaper = [ ",${../../wallpapers/n8ca4obsys991.png}" ];
      };
    };
  };

  systemd.user.services = {
    blueman-applet.Service = { Restart = "on-failure"; RestartSec = "3s"; };
    network-manager-applet.Service = { Restart = "on-failure"; RestartSec = "3s"; };
  };

  wayland.windowManager.hyprland = {
    enable = true;
    settings = {
      input = {
        kb_layout = if ((hostName == "thor") || (hostName == "sif")) then "us,it,us" else "it,us,us";
        kb_options = "grp:menu_toggle,compose:rctrl";
        kb_variant = ",,colemak";
        kb_model = "";
        kb_rules = "";
        numlock_by_default = true;
        follow_mouse = 2;
        touchpad.natural_scroll = true;
      };

      exec = (lib.lists.optionals (hostName == "thor") [
        "xrandr --output DP-1 --primary"
      ]) ++ [
        "noti -t 'Hyprland' -m 'New config loaded\!'"
      ];
      exec-once = [
        # "swww init"
      ];
      # exec-shutdown = [ ];

      env = [
        "XCURSOR_SIZE,24"
      ];

      "$LAPTOP_KEYBOARD_ENABLED" = true;
      device = [
        { name = "at-translated-set-2-keyboard"; enabled = "$LAPTOP_KEYBOARD_ENABLED"; }
        { name = "keychron-k1-max-keyboard"; kb_layout = "us"; }
        { name = "keychron-k1-max-keyboard-2"; kb_layout = "us"; }
      ];

      general = {
        gaps_in = 1;
        gaps_out = 2;
        border_size = 2;
        "col.active_border" = "rgba(${strPalette.normal.yellow}) rgba(${strPalette.normal.red}) 45deg";
        "col.inactive_border" = "rgba(${strPalette.bright.black})";
        layout = "dwindle";
      };

      monitor = [
        ",preferred,auto,auto"
        "Unknown-1,disable"
      ] ++ lib.lists.optionals (hostName == "thor") [
        "DP-1,preferred,1080x200,auto"
        "DP-3,preferred,1080x200,auto"
        "HDMI-A-1,preferred,0x0,auto,transform,3"
      ] ++ lib.lists.optionals (hostName == "odin") [
        "eDP-1,preferred,auto,1"
        "DP-1,preferred,auto-up,1"
      ] ++ lib.lists.optionals (hostName == "sif") [
        "DP-1,preferred,0x0,1"
        "DP-3,preferred,2560x0,1"
        "eDP-1,preferred,1791x1440,1.66667"
      ];

      misc.disable_hyprland_logo = true;

      animations = {
        enabled = true;
        bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";
        animation = [
          "windows, 1, 7, myBezier"
          "windowsOut, 1, 7, default, popin 80%"
          "border, 1, 10, default"
          "borderangle, 1, 8, default"
          "fade, 1, 7, default"
          "workspaces, 1, 6, default"
        ];
      };

      dwindle = {
        #  See https://wiki.hyprland.org/Configuring/Dwindle-Layout/
        # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
        pseudotile = true;
        preserve_split = true; # you probably want this
      };

      master = {
        # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
        new_status = "master";
      };

      gestures = {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more
        workspace_swipe = true;
        workspace_swipe_touch = true;
      };

      decoration = {
        rounding = 10;
        blur = {
          enabled = true;
          size = 3;
          passes = 1;
          new_optimizations = true;
        };

        shadow = {
          enabled = true;
          range = 4;
          render_power = 3;
          color = "rgba(1a1a1aee)";
        };
      };

      "$mod" = "SUPER";

      bind = [
        "SUPER_SHIFT_L, L, exec, hyprctl keyword general:layout dwindle"
        "SUPER_ALT_L, L, exec, hyprctl keyword general:layout master"

        "SUPER, RETURN, exec, kitty"
        "SUPER, W, killactive,"
        "SUPER, M, exec, ${config.programs.hyprlock.package}/bin/hyprlock"
        "SUPER_ALT_L, Q, exit,"
        "SUPER, E, exec, nemo"
        "SUPER, L, exec, ${lib.optionalString (hostName == "thor") "env -u WAYLAND_DISPLAY "}  obsidian"
        "SUPER, V, togglefloating,"
        "SUPER, SPACE, exec, sirula"
        "SUPER, F, fullscreen,"
        "SUPER, P, pseudo, # dwindle"
        "SUPER, J, togglesplit, # dwindle"
        "SUPER ALT, E, exec, wofi-emoji"
        "SUPER ALT, P, exec, wl-clipedit"
        "SUPER, B, exec, wl-update-background"
        ",Print, exec, grimblast copy"
        "SHIFT, Print, exec, grimblast copy area"

        # Move focus to relative position
        "SUPER, left, movefocus, l"
        "SUPER, right, movefocus, r"
        "SUPER, up, movefocus, u"
        "SUPER, down, movefocus, d"

        # Move window to relative position
        "SUPER_SHIFT, left, movewindow, l"
        "SUPER_SHIFT, right, movewindow, r"
        "SUPER_SHIFT, up, movewindow, u"
        "SUPER_SHIFT, down, movewindow, d"

        # Switch workspaces with mainMod + [0-9]
        "SUPER, 1, workspace, 1"
        "SUPER, 2, workspace, 2"
        "SUPER, 3, workspace, 3"
        "SUPER, 4, workspace, 4"
        "SUPER, 5, workspace, 5"
        "SUPER, 6, workspace, 6"
        "SUPER, 7, workspace, 7"
        "SUPER, 8, workspace, 8"
        "SUPER, 9, workspace, 9"
        "SUPER, 0, workspace, 10"

        # Move active window to a workspace with mainMod + SHIFT + [0-9]
        "SUPER SHIFT, 1, movetoworkspace, 1"
        "SUPER SHIFT, 2, movetoworkspace, 2"
        "SUPER SHIFT, 3, movetoworkspace, 3"
        "SUPER SHIFT, 4, movetoworkspace, 4"
        "SUPER SHIFT, 5, movetoworkspace, 5"
        "SUPER SHIFT, 6, movetoworkspace, 6"
        "SUPER SHIFT, 7, movetoworkspace, 7"
        "SUPER SHIFT, 8, movetoworkspace, 8"
        "SUPER SHIFT, 9, movetoworkspace, 9"
        "SUPER SHIFT, 0, movetoworkspace, 10"

        # Move workspace to monitor with mainMod + SHIFT + CTRL + [0-9]
        "SUPER SHIFT CTRL, 1, movecurrentworkspacetomonitor, 0"
        "SUPER SHIFT CTRL, 2, movecurrentworkspacetomonitor, 1"
        "SUPER SHIFT CTRL, 3, movecurrentworkspacetomonitor, 2"
        "SUPER SHIFT CTRL, 4, movecurrentworkspacetomonitor, 3"
        "SUPER SHIFT CTRL, 5, movecurrentworkspacetomonitor, 4"
        "SUPER SHIFT CTRL, 6, movecurrentworkspacetomonitor, 5"
        "SUPER SHIFT CTRL, 7, movecurrentworkspacetomonitor, 6"
        "SUPER SHIFT CTRL, 8, movecurrentworkspacetomonitor, 7"
        "SUPER SHIFT CTRL, 9, movecurrentworkspacetomonitor, 8"
        "SUPER SHIFT CTRL, 0, movecurrentworkspacetomonitor, 9"

        "ALT, Tab, cyclenext"
        "ALT SHIFT, Tab, cyclenext, prev"

        # Scroll through existing workspaces with mainMod + scroll
        "SUPER, mouse_down, workspace, e+1"
        "SUPER, mouse_up, workspace, e-1"
        "SUPER ALT, M, exec, wpctl set-mute @DEFAULT_SOURCE@ toggle"
        ", XF86AudioMicMute, exec, wpctl set-mute @DEFAULT_SOURCE@ toggle"

        ", XF86AudioNext, exec, playerctl next"
        ", XF86AudioPlay, exec, playerctl play-pause"
        ", XF86AudioPrev, exec, playerctl previous"
        "SUPER ALT, Right, exec, playerctl next"
        "SUPER ALT, Down, exec, playerctl play-pause"
        "SUPER ALT, Left, exec, playerctl previous"

        ", XF86PowerOff, exec, wlogout"
      ];

      bindel = [
        ", XF86MonBrightnessUp, exec, brightnessctl -q s 10%+"
        ", XF86MonBrightnessDown, exec, brightnessctl -q s 10%-"
        ", XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 2%+"
        ", XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 2%-"
      ];

      bindl = [
        ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
      ];

      bindm = [
        # mouse movements
        "$mod, mouse:272, movewindow"
        "$mod, mouse:273, resizewindow"
        "$mod ALT, mouse:272, resizewindow"

        # Move/resize windows with mainMod + LMB/RMB and dragging
        "SUPER, mouse:272, movewindow"
        "SUPER, mouse:273, resizewindow"
      ];

      windowrulev2 = [
        "workspace 4, class:(Zotero)"
        "workspace 9, class:(teams-for-linux)"
        "workspace 10, class:(spotify)"
        "tile, class:(Godot)"
        "idleinhibit fullscreen, class:(.*)"
      ];
    };
  };
}
