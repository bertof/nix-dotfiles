{
  programs.gpg = {
    enable = true;
    settings = { };
  };

  services.gpg-agent = {
    enable = true;
    defaultCacheTtl = 600;
    # extraConfig = "allow-loopback-pinentry";
  };
}
