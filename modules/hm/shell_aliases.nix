{
  home.shellAliases = {
    "jc" = "sudo journalctl";
    "jcu" = "journalctl --user";
    "n" = "nix";
    "nb" = "nix build";
    "nf" = "find /nix/store/ -maxdepth 1 | grep";
    "nr" = "nix repl";
    "nR" = "nix run";
    "ns" = "nix search nixpkgs";
    "nsu" = "nix search unstable";
    "nss" = "nix search stable";
    "nS" = "nix shell";
    "sc" = "sudo systemctl";
    "scu" = "systemctl --user";



    "rm" = "rm -i"; # Remove a file
    "cp" = "cp -i"; # Copy a file
    "mv" = "mv -i"; # Move a file
    "dud" = "du -d 1 -h"; # Display the size of files at depth 1 in current location in human-readable form
    "duf*" = "du -sh"; # Display the size of files in current location in human-readable form
    "t" = "tail -f"; # Shorthand for tail which outputs the last part of a file


    "fd*" = "find . -type d -name"; # Find a directory with the given name
    "ff" = "find . -type f -name"; # Find a file with the given name
    "grep" = "grep --color"; # Searches for a query string
    "sgrep" = "grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS}"; # Useful for searching within files


    "h" = "history"; # Lists all recently used commands
    "hgrep" = "fc -El 0 | grep"; # Searches for a word in the list of previously used commands
    "help" = "man"; # Opens up the man page for a command
    "p" = "ps -f"; # Displays currently executing processes
    "sortnr" = "sort -n -r"; # Used to sort the lines of a text file
    "unexport" = "unset"; # Used to unset an environment variable


  };
  # programs.bash = { inherit shellAliases; };
  # programs.zsh = { inherit shellAliases; };
}
