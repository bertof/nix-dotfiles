{ pkgs, ... }:
{
  home.packages = [
    pkgs.lutris
    pkgs.winePackages.full
  ];
}
