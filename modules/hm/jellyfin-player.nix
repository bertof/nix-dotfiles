{ pkgs, ... }:
{
  home.packages = [
    (pkgs.jellyfin-media-player.overrideAttrs (old: {
      postInstall = ''
        ${old.postInstall}
        wrapProgram $out/bin/jellyfinmediaplayer \
          --unset WAYLAND_DISPLAY
      '';
    }))
  ];
}
