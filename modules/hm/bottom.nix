{ pkgs, ... }:
let
  tomlGenerate = (pkgs.formats.toml { }).generate "bottom-toml";
in
{
  home.packages = [ pkgs.bottom ];
  xdg.configFile."bottom/bottom.toml".source = tomlGenerate {
    "flags" = {
      "left_legend" = true;
      "color" = "nord";
    };
  };
}
