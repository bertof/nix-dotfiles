{ pkgs, ... }:
let
  joystickwakeCmd = "${pkgs.joystickwake}/bin/joystickwake";
in
{
  systemd.user.services."joystickwake" = {
    Unit = {
      Description = "Keep lockscreen from activating";
      After = [ "graphical-session.target" ];
      PartOf = [ "graphical-session.target" ];
    };
    Install = {
      WantedBy = [ "graphical-session.target" ];
    };
    Service = {
      Type = "simple";
      ExecStart = joystickwakeCmd;
      Environment = [ "DISPLAY=:0" ];
    };
  };
}
