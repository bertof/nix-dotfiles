{ pkgs, config, ... }:
{
  home.packages = builtins.attrValues { inherit (pkgs) brillo; };

  services.swayidle = {
    enable = true;
    events = [
      {
        event = "before-sleep";
        command = "${config.programs.hyprlock.package}/bin/hyprlock";
      }
      {
        event = "lock";
        command = "${config.programs.hyprlock.package}/bin/hyprlock";
      }
    ];
    timeouts = [
      {
        timeout = 60;
        command = "${pkgs.brillo}/bin/brillo -e -O; ${pkgs.brillo}/bin/brillo -e -S 10 -u 1000000";
        resumeCommand = "${pkgs.brillo}/bin/brillo -e -I";
      }
      {
        timeout = 120;
        command = "${pkgs.brillo}/bin/brillo -e -S 0; ${config.programs.hyprlock.package}/bin/hyprlock";
        resumeCommand = "${pkgs.brillo}/bin/brillo -e -S 100";
      }
    ];
  };
}
