{ nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  strPalette = palette.toRgbaShortHex nixosConfig.nix-rice.rice.colorPalette;
in
{
  programs.hyprlock = {
    enable = true;
    settings = {
      general = {
        ignore_empty_input = true;
        no_fade_in = false;
        no_fade_out = false;
        hide_cursor = true;
      };

      auth = {
        "fingerprint:enabled" = true;
      };

      background = {
        monitor = "";
        path = "screenshot";
        blur_passes = 2;
      };

      label = {
        monitor = "";
        text = "󰌾 $TIME";
        color = "rgba(${strPalette.primary.foreground})";
        font_size = 25;
        font_family = nixosConfig.nix-rice.rice.font.monospace.name;
        rotate = 0;
        position = "85, 100";
        halign = "left";
        valign = "bottom";
      };

      input-field = {
        monitor = "";
        size = "170, 30";
        outline_thickness = 2;
        dots_size = 0.33;
        dots_spacing = 0.15;
        dots_center = true;
        dots_rounding = -1;
        outer_color = "rgba(${strPalette.primary.foreground})";
        inner_color = "rgba(${strPalette.primary.background})";
        font_color = "rgba(${strPalette.primary.foreground})";
        fade_on_empty = true;
        fade_timeout = 1000;
        placeholder_text = "";
        hide_input = false;
        rounding = -1;
        check_color = "rgba(${strPalette.normal.yellow})";
        fail_color = "rgba(${strPalette.normal.yellow})";
        fail_text = "Wrong password";
        fail_transition = 200;
        capslock_color = -1;
        numlock_color = -1;
        bothlock_color = -1;
        invert_numlock = false;
        swap_font_color = false;
        position = "60, 50";
        halign = "left";
        valign = "bottom";
      };
    };
  };
}
