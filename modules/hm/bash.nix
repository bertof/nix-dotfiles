{
  programs.bash = {
    enable = true;
    enableVteIntegration = true;
    shellAliases = {
      ".." = "cd ..";
    };

    bashrcExtra = ''
      source $HOME/.profile
    '';
  };
}
