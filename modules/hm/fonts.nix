{ pkgs, ... }:
{
  fonts.fontconfig.enable = true;
  home.packages = builtins.attrValues {
    inherit (pkgs)
      dejavu_fonts
      noto-fonts
      noto-fonts-extra
      noto-fonts-cjk-sans
      font-awesome
      corefonts# Microsoft fonts
      vistafonts
      ;
  };
}
