{ pkgs, nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  inherit (nixosConfig.nix-rice) rice;
  strPalette = palette.toRgbHex {
    inherit (rice.colorPalette) normal bright dark;
    inherit (rice.colorPalette.primary) background foreground;
  };
  opacity = toString rice.opacity;
  font = rice.font.monospace;
  colorString =
    normal: bright:
    builtins.concatStringsSep ":" [
      normal.black
      normal.red
      normal.green
      normal.yellow
      normal.blue
      normal.magenta
      normal.cyan
      normal.white
      bright.black
      bright.red
      bright.green
      bright.yellow
      bright.blue
      bright.magenta
      bright.cyan
      bright.white
    ];
in
{
  home.packages = [ pkgs.terminator ];

  xdg.configFile."terminator/config".text = ''
    [global_config]
      scroll_tabbar = True
      enabled_plugins = ActivityWatch, LaunchpadCodeURLHandler, APTURLHandler, LaunchpadBugURLHandler
      suppress_multiple_term_dialog = True
      always_split_with_profile = True
    [keybindings]
      help = None
    [profiles]
      [[default]]
        visible_bell = True
        background_color = "${strPalette.background}"
        background_darkness = ${opacity}
        background_type = transparent
        cursor_color = "${strPalette.foreground}"
        font = ${font.name} weight=450 ${toString font.size}
        foreground_color = "${strPalette.foreground}"
        show_titlebar = False
        scrollbar_position = hidden
        scrollback_lines = 10000
        palette = "${colorString strPalette.normal strPalette.bright}"
        use_system_font = False
      [[Light presentation]]
        visible_bell = True
        background_color = "${strPalette.foreground}"
        cursor_color = "${strPalette.background}"
        font = ${font.name} weight=450 20
        foreground_color = "${strPalette.background}"
        show_titlebar = False
        palette = "${colorString strPalette.dark strPalette.normal}"
        use_system_font = False
      [[Dark presentation]]
        visible_bell = True
        background_color = "${strPalette.background}"
        cursor_color = "${strPalette.foreground}"
        font = ${font.name} weight=450 20
        foreground_color = "${strPalette.foreground}"
        show_titlebar = False
        palette = "${colorString strPalette.normal strPalette.bright}"
        use_system_font = False
    [layouts]
      [[default]]
        [[[child1]]]
          parent = window0
          type = Terminal
          profile = default
        [[[window0]]]
          parent = ""
          type = Window
    [plugins]
  '';
}
