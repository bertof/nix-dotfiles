{ pkgs, ... }:
let
  onedriveCmd = "${pkgs.onedrive}/bin/onedrive --monitor";
in
{
  systemd.user.services."onedrive" = {
    Unit = {
      Description = "OneDrive synchronization service";
      After = [ "network.target" ];
      PartOf = [ "default.target" ];
    };
    Install = {
      WantedBy = [ "default.target" ];
    };
    Service = {
      ExecStart = onedriveCmd;
      Restart = "always";
      RestartSec = "30s";
    };
  };
}
