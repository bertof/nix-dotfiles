{ pkgs, nixosConfig, ... }:
with nixosConfig.nix-rice.lib;
let
  inherit (nixosConfig.nix-rice) rice;
  strPalette = palette.toRGBHex rice.colorPalette;
in
{
  home.packages = builtins.attrValues {
    inherit (pkgs) dunst;
    inherit (rice.font.normal) package;
  };
  services.dunst = {
    enable = true;
    iconTheme = { package = pkgs.qogir-icon-theme; name = "Qogir"; };
    settings = {
      global = {
        browser = "${pkgs.xdg-utils}/bin/xdg-open";
        corner_radius = 10;
        dmenu = "${pkgs.rofi}/bin/rofi -dmenu -p dunst:";
        follow = "keyboard";
        font = "${rice.font.normal.name} 10";
        frame_color = strPalette.normal.blue;
        frame_width = 1;
        height = 320;
        mouse_middle_click = "context";
        notification_limit = 10;
        offset = "7x7";
        separator_color = strPalette.normal.blue;
        transparency = 100 - nix-rice.float.round (rice.opacity * 100);
        width = "(0, 320)";
      };
      urgency_low = {
        inherit (strPalette) background foreground;
        timeout = 10;
      };
      urgency_normal = {
        inherit (strPalette) background foreground;
        timeout = 10;
      };
      urgency_critical = {
        inherit (strPalette) background foreground;
        frame_color = strPalette.bright.red;
        timeout = 0;
      };
      shortcuts = {
        # close = "ctrl+space";
        # close_all = "ctrl+shift+space";
        # history = "ctrl+backslash";
        # context = "ctrl+shift+period";
      };
    };
  };
}
