local lspconfig = require('lspconfig')
local capabilities = require("cmp_nvim_lsp").default_capabilities()
local lsp_format = require("lsp-format")

-- Keymaps
local keymap = vim.keymap
local opts = { noremap = true, silent = true }
local on_attach = function(client, bufnr)
  opts.buffer = bufnr

  lsp_format.on_attach(client, bufrn)

  opts.desc = "Show LSP references"
  keymap.set("n", "gR", "<cmd>Telescope lsp_references<CR>", opts)
  opts.desc = "Go to declaration"
  keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
  opts.desc = "Go to definition"
  keymap.set("n", "gd", "<cmd>Telescope lsp_definitions<CR>", opts)
  opts.desc = "Show LSP implementations"
  keymap.set("n", "gi", "<cmd>Telescope lsp_implementations<CR>", opts)
  opts.desc = "Show LSP type definitions"
  keymap.set("n", "gt", "<cmd>Telescope lsp_type_definitions<CR>", opts)
  opts.desc = "See available code actions"
  keymap.set({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, opts)
  opts.desc = "Smart rename"
  keymap.set("n", "<leader>rn", vim.lsp.buf.rename, opts)
  opts.desc = "Smart show diagnostics"
  keymap.set("n", "<leader>D", "<cmd>Telescope diagnostics", opts)
  opts.desc = "Smart show buffer diagnostics"
  keymap.set("n", "<leader>D", "<cmd>Telescope diagnostics bufnr=0<CR>", opts)
  opts.desc = "Smart show line diagnostics"
  keymap.set("n", "<leader>d", vim.diagnostic.open_float, opts)
  opts.desc = "Go to previous diagnostic"
  keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
  opts.desc = "Go to next diagnostic"
  keymap.set("n", "]d", vim.diagnostic.goto_next, opts)
  opts.desc = "Show documentation for what is under cursor"
  keymap.set("n", "K", vim.lsp.buf.hover, opts)
  opts.desc = "Restart LSP"
  keymap.set("n", "<leader>rs", ":LspRestart<CR>", opts)
end

-- Change the Diagnostic symbols in the sign column (gutter)
-- (not in youtube nvim video)
local signs = { Error = " ", Warn = " ", Hint = "󰠠 ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end

-- LSP configuration
lspconfig["html"].setup { capabilities = capabilities, on_attach = on_attach}
lspconfig["gopls"].setup { capabilities = capabilities, on_attach = on_attach}
lspconfig["pylsp"].setup { capabilities = capabilities, on_attach = on_attach}
lspconfig["nil_ls"].setup { capabilities = capabilities, on_attach = on_attach, settings = {
  ["nil"] = {
    formatting = { command = { "nixpkgs-fmt" } },
    autoEvalInputs = true
  }
}}
lspconfig["rust_analyzer"].setup { capabilities = capabilities, on_attach = on_attach}
lspconfig["texlab"].setup { capabilities = capabilities, on_attach = on_attach, settings = {
  texlab = {
    formatterLineLength = 0,
    bibtexFormatter = "latexindent",
    latexindent = { modifyLineBreaks = false },
    chktex = { onEdit = true },
  }
}}
lspconfig["clangd"].setup { capabilities = capabilities, on_attach = on_attach}
lspconfig["marksman"].setup { capabilities = capabilities, on_attach = on_attach}
lspconfig["yamlls"].setup { capabilities = capabilities, on_attach = on_attach, settings = {
  yaml = {
    keyOrdering = false,
    schemas = {
      ["https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json"] = "*gitlab-ci*.{yml,yaml}",
      ["https://json.schemastore.org/ansible-playbook"] = "*play*.{yml,yaml}",
      ["https://json.schemastore.org/ansible-stable-2.9"] = "roles/tasks/*.{yml,yaml}",
      ["https://json.schemastore.org/chart"] = "Chart.{yml,yaml}",
      ["https://json.schemastore.org/dependabot-v2"] = ".github/dependabot.{yml,yaml}",
      ["https://json.schemastore.org/github-action"] = ".github/action.{yml,yaml}",
      ["https://json.schemastore.org/github-workflow"] = ".github/workflows/*",
      ["https://json.schemastore.org/hugo"] = "hugo.{yml,yaml,toml}",
      ["https://json.schemastore.org/kustomization"] = "kustomization.{yml,yaml}",
      ["https://json.schemastore.org/prettierrc"] = ".prettierrc.{yml,yaml}",
      ["https://raw.githubusercontent.com/OAI/OpenAPI-Specification/main/schemas/v3.1/schema.json"] = "*api*.{yml,yaml}",
      ["https://raw.githubusercontent.com/argoproj/argo-workflows/master/api/jsonschema/schema.json"] = "*flow*.{yml,yaml}",
      ["https://raw.githubusercontent.com/compose-spec/compose-spec/master/schema/compose-spec.json"] = "*docker-compose*.{yml,yaml}",
      ["kubernetes"] = "*.yaml",
    }
  }
}}
lspconfig["pylsp"].setup({
  capabilities = capabilities,
  on_attach = on_attach,
})
-- lspconfig["cmake-language-server"].setup { capabilities = capabilities, on_attach = on_attach}
-- lspconfig["vscode-css-language-server"].setup { capabilities = capabilities, on_attach = on_attach}
