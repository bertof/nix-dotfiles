{
  programs.autorandr = {
    enable = true;
    profiles =
      let
        dell-laptop = {
          dpi = 96;
          fingerprint = "00ffffffffffff0030e43f0500000000001a010495221378eaa1c59459578f27205054000000010101010101010101010101010101012e3680a070381f403020350058c21000001a5c2b80a070381f403020350058c21000001a000000fe0034584b3133803135365746360a000000000000413196001000000a010a202000c5";
          mode = "1920x1080";
        };
        schermo_lab_dp = {
          dpi = 96;
          fingerprint = "00ffffffffffff0010ac73404c47334428160104a5331d783bdd45a3554fa027125054a54b00714f8180d1c001010101010101010101023a801871382d40582c4500fe1f1100001e000000ff004b463837593241364433474c0a000000fc0044454c4c205532333132484d0a000000fd00384c1e5311000a2020202020200096";
          mode = "1920x1080";
        };
        lab_screen = {
          dpi = 96;
          fingerprint = "00ffffffffffff0010acd3a14246313216200104a53b21783be755a9544aa1260d5054a54b00714f8180a9c0d1c00101010101010101565e00a0a0a029503020350055502100001a000000ff00435958564744330a2020202020000000fc0044454c4c20533237323244430a000000fd00304b72723c010a20202020202001d9020325f14f101f051404131211030216150706012309070783010000681a00000101304b007e3900a080381f4030203a0055502100001a023a801871382d40582c450055502100001ed97600a0a0a034503020350055502100001a00000000000000000000000000000000000000000000000000000000000000000000000092";
          mode = "2560x1440";
        };
      in
      {
        odin-nvidia = {
          fingerprint = {
            "eDP-1-1" = dell-laptop.fingerprint;
          };
          config = {
            "eDP-1-1" = {
              inherit (dell-laptop) mode dpi;
              primary = true;
            };
          };
        };

        odin-intel = {
          fingerprint = {
            "eDP1" = dell-laptop.fingerprint;
          };
          config = {
            "eDP1" = {
              inherit (dell-laptop) mode dpi;
              primary = true;
            };
          };
        };

        odin-intel-lab-dock = {
          fingerprint = {
            "DP1-2" = schermo_lab_dp.fingerprint;
            "eDP1" = dell-laptop.fingerprint;
          };
          config = {
            "DP1-2" = {
              inherit (schermo_lab_dp) mode dpi;
              primary = true;
            };
            "eDP1" = {
              inherit (dell-laptop) mode dpi;
              position = "0x1080";
            };
          };
        };

        odin-nvidia-lab-dock = {
          fingerprint = {
            "DP-1-1-2" = schermo_lab_dp.fingerprint;
            "eDP-1-1" = dell-laptop.fingerprint;
          };
          config = {
            "DP-1-1-2" = {
              inherit (schermo_lab_dp) mode dpi;
              primary = true;
            };
            "eDP-1-1" = {
              inherit (dell-laptop) mode dpi;
              position = "0x1080";
            };
          };
        };

        odin-intel-lab-screen = {
          fingerprint = {
            "DP1" = lab_screen.fingerprint;
            "eDP1" = dell-laptop.fingerprint;
          };
          config = {
            "DP1" = {
              inherit (lab_screen) mode dpi;
              primary = true;
            };
            "eDP1" = {
              inherit (dell-laptop) mode dpi;
              position = "320x1440";
            };
          };
        };

        odin-nvidia-lab-screen = {
          fingerprint = {
            "DP-1-1" = lab_screen.fingerprint;
            "eDP-1-1" = dell-laptop.fingerprint;
          };
          config = {
            "DP-1-1" = {
              inherit (lab_screen) mode dpi;
              primary = true;
            };
            "eDP-1-1" = {
              inherit (dell-laptop) mode dpi;
              position = "320x1440";
            };
          };
        };

        thor-default = {
          fingerprint = {
            "DP-3" = "00ffffffffffff001e6d7f5ba1c60a00011f0104b53c22789f8cb5af4f43ab260e5054254b007140818081c0a9c0b300d1c08100d1cf09ec00a0a0a0675030203a0055502100001a000000fd0030901ee63c000a202020202020000000fc004c4720554c545241474541520a000000ff003130314e5454514c533230390a017102031a7123090607e305c000e606050160592846100403011f136fc200a0a0a0555030203a0055502100001a565e00a0a0a029503020350055502100001a5aa000a0a0a0465030203a005550210000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000da";
            "HDMI-0" = "00ffffffffffff004c2d2c0d443650302e19010380341d782a5295a556549d250e5054bb8c00b30081c0810081809500a9c001010101023a801871382d40582c450009252100001e000000fd0030481e5216000a202020202020000000fc00433234463339300a2020202020000000ff00485451483330353636370a20200163020324f14690041f131203230907078301000067030c001000802b681a00000101304800011d00bc52d01e20b828554009252100001e8c0ad090204031200c4055000925210000188c0ad08a20e02d10103e9600092521000018215280a07238304088c8350009252100001c00000000000000000000000000000000000000fa";
          };
          config = {
            "DP-3" = {
              mode = "2560x1440";
              position = "1080x346";
              primary = true;
              dpi = 96;
            };
            "HDMI-0" = {
              mode = "1920x1080";
              position = "0x0";
              rotate = "right";
              dpi = 96;
            };
          };
        };
      };
  };
}
