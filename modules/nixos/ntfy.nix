let
  port = 7080;
in
{
  services.ntfy-sh = {
    enable = true;
    settings = {
      listen-http = ":${toString port}";
      base-url = "https://ntfy.bertof.net";
      behind-proxy = true;
      auth-default-access = "deny-all";
    };
  };

  networking.firewall.allowedTCPPorts = [ port ];
}
