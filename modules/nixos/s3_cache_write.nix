{ config, lib, ... }: {
  age.secrets.s3_cache_write.file = ../../secrets/s3_cache_write.age;
  systemd.services.nix-daemon.serviceConfig.EnvironmentFile = lib.mkForce config.age.secrets.s3_cache_write.path;
}

