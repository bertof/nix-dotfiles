{ pkgs, config, lib, ... }:
with config.nix-rice.lib;
let
  theme = kitty-themes.getThemeByName "Nightfox";
  inherit (config.nix-rice) rice;
in
{
  nix-rice.config = {
    colorPalette = rec {
      normal = palette.defaultPalette // {
        black = theme.color0;
        red = theme.color1;
        green = theme.color2;
        yellow = theme.color3;
        blue = theme.color4;
        magenta = theme.color5;
        cyan = theme.color6;
        white = theme.color7;
      };
      bright = palette.brighten 10 normal // {
        black = theme.color8;
        red = theme.color9;
        green = theme.color10;
        yellow = theme.color11;
        blue = theme.color12;
        magenta = theme.color13;
        cyan = theme.color14;
        white = theme.color15;
      };
      dark = palette.darken 10 normal;
      primary = {
        inherit (theme) background foreground;
        bright_foreground = color.brighten 10 theme.foreground;
        dim_foreground = color.darken 10 theme.foreground;
      };
    } // theme;
    font = {
      normal = {
        name = "Cantarell";
        package = pkgs.cantarell-fonts;
        size = 10;
      };
      monospace = {
        name = "CaskaydiaCove Nerd Font";
        package = pkgs.nerd-fonts.caskaydia-mono or (pkgs.nerdfonts.override { fonts = [ "CascadiaCode" ]; });
        # package = pkgs.nerdfonts.override { fonts = [ "CascadiaCode" ]; };
        # name = "FiraCode Nerd Font Mono";
        # package = pkgs.nerdfonts.override { fonts = [ "FiraCode" ]; };
        size = 10;
      };
    };
    opacity = 0.95;
  };

  nixpkgs.overlays = lib.mkAfter [
    (_self: super: {
      wl-lockscreen = super.wl-lockscreen.override {
        strPalette = palette.toRgbaShortHex rice.colorPalette;
        font = rice.font.normal;
      };
    })
  ];
}
