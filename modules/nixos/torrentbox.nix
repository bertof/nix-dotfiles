{ pkgs, ... }: {
  services = {
    # ombi = { enable = true; openFirewall = true; group = "users"; };

    bazarr = {
      enable = true;
      openFirewall = true;
      group = "users";
    };
    lidarr = {
      enable = true;
      openFirewall = true;
      group = "users";
    };
    prowlarr = {
      enable = true;
      openFirewall = true;
    };
    radarr = {
      enable = true;
      openFirewall = true;
      group = "users";
    };
    readarr = {
      enable = true;
      openFirewall = true;
      group = "users";
    };
    sonarr = {
      enable = true;
      openFirewall = true;
      group = "users";
    };

    transmission = {
      enable = true;
      openFirewall = true;
      group = "users";
      package = pkgs.transmission_4;
      settings.download-dir = "/mnt/raid/condiviso/Torrent";
    };
  };

  systemd.services = {
    # ombi.serviceConfig = { MemoryHigh = "400M"; MemoryMax = "1G"; };

    bazarr.serviceConfig = {
      MemoryHigh = "600M";
      MemoryMax = "2G";
    };
    lidarr.serviceConfig = {
      MemoryHigh = "600M";
      MemoryMax = "2G";
    };
    prowlarr.serviceConfig = {
      MemoryHigh = "600M";
      MemoryMax = "2G";
    };
    radarr.serviceConfig = {
      MemoryHigh = "600M";
      MemoryMax = "2G";
    };
    readarr.serviceConfig = {
      MemoryHigh = "600M";
      MemoryMax = "2G";
    };
    sonarr.serviceConfig = {
      MemoryHigh = "600M";
      MemoryMax = "2G";
    };

    transmission.serviceConfig = {
      MemoryHigh = "400M";
      MemoryMax = "1G";
    };
  };
}
