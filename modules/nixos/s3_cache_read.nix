{ config, ... }: {
  age.secrets.s3_cache_read.file = ../../secrets/s3_cache_read.age;
  systemd.services.nix-daemon.serviceConfig.EnvironmentFile = config.age.secrets.s3_cache_read.path;
}
