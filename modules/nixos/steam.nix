{ pkgs, ... }: {
  programs.steam = {
    enable = true;

    package = pkgs.steam.override {
      # extraLibraries = pkgs: with pkgs; [
      #   dconf.lib
      #   gnome.gvfs
      #   # icu
      #   # libdrm
      # ];
      # extraPkgs = pkgs: with pkgs; [
      #   # icu
      # ];
      # extraProfile = ''
      #   export GSETTINGS_SCHEMA_DIR="${pkgs.gsettings-desktop-schemas}/share/gsettings-schemas/${pkgs.gsettings-desktop-schemas.name}/glib-2.0/schemas/"
      # '';
    };
    extraPackages = [
      pkgs.gamescope
    ];
    extraCompatPackages = [
      pkgs.proton-ge-bin
    ];
    remotePlay.openFirewall = true;
    extest.enable = true;
  };

  fonts.packages = [
    pkgs.liberation_ttf
    pkgs.wqy_zenhei
  ];

  # networking.firewall = {
  #   allowedTCPPorts = [
  #     27036
  #     27037
  #   ];
  #   allowedUDPPorts = [
  #     27031
  #     27036
  #   ];
  # };
}
