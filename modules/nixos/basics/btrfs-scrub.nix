{ lib, config, ... }:
let
  inherit (builtins) mapAttrs attrValues;
  inherit (lib) filterAttrs unique;
  btrfsFileSystems = filterAttrs (_k: v: v.fsType == "btrfs") config.fileSystems;
  btrfsDevices = unique (attrValues (mapAttrs (_: v: v.device) btrfsFileSystems));
in
{
  services.btrfs.autoScrub = {
    enable = btrfsDevices != [ ];
    fileSystems = btrfsDevices;
  };
}
