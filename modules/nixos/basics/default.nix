{
  imports = [
    ./automatic-garbage-collection.nix
    ./btrfs-scrub.nix
    ./distributed.nix
    ./extended-registry.nix
    ./fstrim.nix
    ./fwupd.nix
    ./remote-deploy.nix
    ./tailscale.nix
    ./zerotier.nix
  ];
}
