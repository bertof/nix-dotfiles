{ lib, ... }:
let
  hosts = import ../../../hosts.nix;
  zerotier_hosts = lib.attrsets.mapAttrs'
    (
      k: v: lib.attrsets.nameValuePair v [ k ]
    )
    hosts.zerotier.ipv4;
in
{
  services.zerotierone = {
    enable = true;
    joinNetworks = [ "8056c2e21cf9c753" ];
  };

  networking.hosts = zerotier_hosts;
}
