{
  nix.registry = {
    agenix = {
      from = {
        id = "agenix";
        type = "indirect";
      };
      to = {
        type = "github";
        owner = "ryantm";
        repo = "agenix";
      };
    };
    ragenix = {
      from = {
        id = "ragenix";
        type = "indirect";
      };
      to = {
        type = "github";
        owner = "yaxitech";
        repo = "ragenix";
      };
    };
    my-templates = {
      from = {
        id = "my-templates";
        type = "indirect";
      };
      to = {
        type = "github";
        owner = "bertof";
        repo = "flake-templates";
      };
    };
    tex2nix = {
      from = {
        id = "tex2nix";
        type = "indirect";
      };
      to = {
        type = "github";
        owner = "Mic92";
        repo = "tex2nix";
      };
    };
  };
}
