{ lib, ... }:
let
  hosts = import ../../../hosts.nix;
  tailscale_hosts = lib.attrsets.mapAttrs'
    (
      k: v: lib.attrsets.nameValuePair v [ k ]
    )
    hosts.tailscale.ipv4;
in
{
  services.tailscale = {
    enable = true;
    permitCertUid = "filippoberto95@gmail.com";
  };

  networking.firewall.checkReversePath = "loose";

  networking.hosts = tailscale_hosts;
}
