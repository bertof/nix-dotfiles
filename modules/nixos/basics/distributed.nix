{
  # security.sudo.wheelNeedsPassword = false;
  nix.settings = {
    extra-substituters = [
      "https://hyprland.cachix.org"
      "s3://nix-cache?endpoint=heimdall.tsn:9000&scheme=http&parallel-compression=true&want-mass-query=true&priority=35"
      # "s3://nix-cache?endpoint=s3.bertof.net"
    ];

    trusted-users = [
      "root"
      "@wheel"
    ];

    trusted-public-keys = [
      "thor:yRx3HglIxjUYocp4/jAP9dPWxWBEpgP6hqj1ofEfn1A="
      "odin:ClRXzxmDZl2Y94SG4YlWXGiJDY4L9DgZq/3OLR5+i6k="
      "loki:HN1P2nXzIkqitl95MvjcSHxtDo7Ao+I8M8U/RqQLC5k="
      "baldur:iP+Cg3JIcjYES9cmk+nnpd+7Po+rPlwVKqpBOAyrD64="

      "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
    ];

    secret-key-files = [ "/etc/nix/key" ];

    # substituters = [
    #   "ssh-ng://thor.local"
    #   "ssh-ng://odin.local"
    #   "ssh-ng://loki.local"
    # ];
  };
}
