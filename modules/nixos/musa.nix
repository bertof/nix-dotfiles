{
  networking.extraHosts = ''
    172.20.28.150 airflow.musa.sesar.di.unimi.it
    172.20.28.150 conjur.musa.sesar.di.unimi.it
    172.20.28.150 harbor.musa.sesar.di.unimi.it
    172.20.28.150 httpfs.musa.sesar.di.unimi.it
    172.20.28.150 jupyterhub.musa.sesar.di.unimi.it
    172.20.28.150 kerberos.musa.sesar.di.unimi.it
    172.20.28.150 keycloak.musa.sesar.di.unimi.it
    172.20.28.150 livy.musa.sesar.di.unimi.it
    172.20.28.150 oauth2-proxy.musa.sesar.di.unimi.it
    172.20.28.150 opensearch-dashboards.musa.sesar.di.unimi.it
    172.20.28.150 opensearch.musa.sesar.di.unimi.it
    172.20.28.150 phpldapadmin.musa.sesar.di.unimi.it
    172.20.28.150 rancher.musa.sesar.di.unimi.it
    172.20.28.150 ranger.musa.sesar.di.unimi.it
    172.20.28.150 recon.musa.sesar.di.unimi.it
    172.20.28.150 spark-history.musa.sesar.di.unimi.it
    172.20.28.150 trino.musa.sesar.di.unimi.it
  '';
}
