{ config, ... }: {
  services.openssh = {
    enable = true;
    openFirewall = true;
    settings = {
      # PermitRootLogin = "prohibit-password";
      PasswordAuthentication = false;
    };
  };

  users.users.root.openssh.authorizedKeys.keys = config.users.users.bertof.openssh.authorizedKeys.keys;

  system.stateVersion = "22.11";
}
