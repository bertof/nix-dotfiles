{ lib, config, ... }:
{

  age.secrets.kavita_token = {
    file = ../../secrets/kavita_token.age;
    owner = "kavita";
  };

  services.kavita = {
    enable = true;
    tokenKeyFile = config.age.secrets.kavita_token.path;
  };

  networking.firewall.allowedTCPPorts = lib.optionals config.services.kavita.enable [
    config.services.kavita.port
  ];
}
