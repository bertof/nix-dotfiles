{ config
, lib
, ...
}:
{
  users.groups.garage = { };
  users.users.garage = {
    isSystemUser = true;
    group = "garage";
  };
  age.secrets.garage_rpc_secret = {
    file = ../../secrets/garage_rpc_secret.age;
    owner = "garage";
  };

  networking.firewall.allowedTCPPorts = [
    3900
    3901
  ];

  # Not correctly passing mount bindings
  systemd.services.garage.serviceConfig = {
    ProtectHome = lib.mkForce false;
    DynamicUser = false;
  };

  services.garage = {
    enable = true;
    settings = {
      replication_mode = 1;
      rpc_secret_file = config.age.secrets.garage_rpc_secret.path;
      rpc_bind_addr = "0.0.0.0:3901";
      bootstrap_peers = [ ];

      s3_api = {
        api_bind_addr = "0.0.0.0:3900";
        s3_region = "garage";
        root_domain = ".s3.bertof.net";
      };

      s3_web = {
        bind_addr = "0.0.0.0:3902";
        root_domain = ".web.bertof.net";
      };

      admin = {
        api_bind_addr = "0.0.0.0:3903";
        # metrics_token = "72ad105afc44f30c189b2505f5583d3ea9be26a3e0a4730d48381b1ae4b70074";
        # admin_token = "05bf164fe1ce3ecc1dff8fb1e5b237331d24b109792be714738fa92b2d14213d";
        # trace_sink = "http://localhost:4317";
      };
    };
  };
}
