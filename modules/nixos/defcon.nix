{
  networking.hosts = {
    "54.176.11.243" = [ "vpn.mhackeroni.it" ];
    "10.100.0.50" = [
      "master.cb.cloud.mhackeroni.it"
      "bartender.cb.cloud.mhackeroni.it"
      "grafana.cb.cloud.mhackeroni.it"
      "menu.cb.cloud.mhackeroni.it"
      "maitre.cb.cloud.mhackeroni.it"
      "accountant.cb.cloud.mhackeroni.it"
    ];
    "10.100.0.150" = [
      "flowgui.cloud.mhackeroni.it"
      "smb.cloud.mhackeroni.it"
    ];
    "10.100.0.200" = [ "tunniceddu.cloud.mhackeroni.it" ];
    "10.100.0.250" = [ "rev.cloud.mhackeroni.it" ];
    "10.100.0.66" = [ "attackerbackup.cloud.mhackeroni.it" ];
    "192.168.128.1" = [
      "smb.hotel.mhackeroni.it"
      "rev.hotel.mhackeroni.it"
    ];
  };
}
