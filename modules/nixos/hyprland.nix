{ config, ... }:
{
  programs.hyprland = {
    enable = true;
    # enableNvidiaPatches = builtins.elem "nvidia" config.services.xserver.videoDrivers; # removed in 24.x
    xwayland.enable = true;
  };

  environment.sessionVariables = {
    WLR_NO_HARDWARE_CURSORS = "1";
    NIXOS_OZONE_WL = "1";
  };

  hardware = {
    graphics.enable = true;
    nvidia.modesetting.enable = builtins.elem "nvidia" config.services.xserver.videoDrivers;
  };

  services.pipewire.wireplumber.enable = true;
  security.pam.services.hyprlock = { };

  security.pam.services.swaylock = {
    text = ''
      auth include login
    '';
  };
}
