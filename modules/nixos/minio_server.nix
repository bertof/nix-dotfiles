{ config, ... }:
{
  age.secrets.minio = {
    file = ../../secrets/minio.age;
    owner = "minio";
  };

  services.minio = {
    enable = true;
    dataDir = [
      # "/var/lib/minio/data"
      "/mnt/raid/minio/data/"
    ];
    rootCredentialsFile = config.age.secrets.minio.path;
  };
}
