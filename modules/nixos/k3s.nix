{
  services.k3s = {
    enable = true;
  };

  networking.firewall = {
    allowedTCPPorts = [
      6443 # Kubernetes API
    ];
    allowedTCPPortRanges = [
      {
        from = 9000;
        to = 15000;
      }
    ];
  };
}
