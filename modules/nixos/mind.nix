{
  services.postgresql = {
    enable = true;
    ensureDatabases = [ "mfh" ];
    ensureUsers = [
      {
        name = "bertof";
        ensurePermissions = {
          "DATABASE \"mfh\"" = "ALL PRIVILEGES";
        };
      }
    ];
  };

  services.apache-kafka = {
    enable = true;
  };
}
