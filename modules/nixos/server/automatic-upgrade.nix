{
  system.autoUpgrade = {
    enable = true;
    flake = "gitlab:bertof/nix-dotfiles";
    # dates = "daily"; # default 04:04
    randomizedDelaySec = "45min";
    flags = [ "--refresh" ];
    # rebootWindow = { upper = "06:00"; lower = "02:00"; };
  };
}
