{ config, ... }:
{
  age.secrets.minio = {
    file = ../../secrets/minio.age;
    owner = "minio";
  };

  services.minio = {
    enable = true;
    rootCredentialsFile = config.age.secrets.minio.path;
  };
}

