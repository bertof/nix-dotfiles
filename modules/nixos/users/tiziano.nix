{
  users.users.tiziano = {
    isNormalUser = true;
    extraGroups = [ ];
    openssh.authorizedKeys.keys = [
      # "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDUZQHsId/If3Gyp3ReUixOHTISHHKR8qIyZw3cg6NXr tiziano@loki"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBizLl/vBLgRpQiGCr2U5rLFkYEbOgQqC5IUVlV3PV37 turri@HPG5"
    ];
  };
}
