{ pkgs, ... }: {
  users.users.bertof = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [
      "audio"
      "dialout"
      "docker"
      "flashrom"
      "input"
      "kvm"
      "libvirtd"
      "network"
      "networkmanager"
      "tss"
      "usb"
      "video"
      "wheel"
    ];
    openssh.authorizedKeys.keys = [
      # "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC3W3Btk1qtLHU69aFwseDuKU6PJMA+NxVXJXiRNhDce bertof@odin"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO7mcf8fbMo1eXqSJeVFWaweB+JOU+67dFuf8laZKZZG bertof@thor"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKT+D5QE4TkgoKw5IvSYpvnvIIRM87RBePHce1Aaz3xJ bertof@thor"
      # "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKbG791lSOl8Rqoy+KkdKiOJnOMRg02+HZ/VrlrWMYAX bertof@baldur"
      # "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFviqAN0S+wZ5BQRpWpmsrkduPox3L4C7iLlCOQk7+pE bertof@loki"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFWnGoScIwOUQurZx8j0Y18nxdUJ3/gNyP5vleKkS/00 bertof@sif"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKobKuuJCoQ7gj/NeE57wfSg/Qs4X3osw9xXook3PMAP bertof@extra"
    ];
  };
  programs.zsh.enable = true;
}
