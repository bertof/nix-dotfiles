{ pkgs, ... }:

let
  drivers = pkgs.xp-pen-deco-01-v2-driver.overrideAttrs (_o: rec {
    version = "3.2.3.230215-1";
    src = pkgs.fetchzip {
      urls = [
        "https://download01.xp-pen.com/file/2023/03/XPPen-pentablet-${version}.x86_64.tar.gz"
        "https://web.archive.org/web/20230424112207/https://download01.xp-pen.com/file/2023/03/XPPen-pentablet-${version}.x86_64.tar.gz"
      ];
      name = "xp-pen-deco-01-v2-driver-${version}.tar.gz";
      sha256 = "sha256-CV4ZaGCFFcfy2J0O8leYgcyzFVwJQFQJsShOv9B7jfI=";
    };
  });
in
{
  # udev rules
  services.udev.extraRules = ''
    KERNEL=="uinput",MODE:="0666",OPTIONS+="static_node=uinput"
    SUBSYSTEMS=="usb",ATTRS{idVendor}=="28bd",MODE:="0666"
  '';

  # XP-Pen tablet driver
  environment.systemPackages = [ drivers ];

  # hardware.opentabletdriver = {
  #   enable = true;
  #   daemon.enable = true;
  # };
}
