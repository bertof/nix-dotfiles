{ pkgs, config, ... }:
let
  hosts = import ../../hosts.nix;
in
{

  age.secrets = {
    nextcloud_admin_secret = {
      file = ../../secrets/nextcloud_admin_secret.age;
      owner = "nextcloud";
      group = "nextcloud";
    };
    nextcloud_bucket_secret = {
      file = ../../secrets/nextcloud_bucket_secret.age;
      owner = "nextcloud";
      group = "nextcloud";
    };
  };

  # services.nginx.virtualHosts.${config.services.nextcloud.hostName} = {
  #   enableACME = true;
  #   forceSSL = true;
  # };

  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud30;

    hostName = "my-nextcloud.bertof.net";
    maxUploadSize = "24G";
    caching.apcu = true;

    database.createLocally = true;

    extraApps = {
      inherit (config.services.nextcloud.package.packages.apps)
        contacts
        calendar
        notes
        # maps
        # memories
        tasks
        # richdocuments
        ;
    };
    appstoreEnable = true;
    autoUpdateApps.enable = true;
    settings = {

      enabledPreviewProviders = [
        "OC\\Preview\\BMP"
        "OC\\Preview\\GIF"
        "OC\\Preview\\JPEG"
        "OC\\Preview\\Krita"
        "OC\\Preview\\MarkDown"
        "OC\\Preview\\MP3"
        "OC\\Preview\\OpenDocument"
        "OC\\Preview\\PNG"
        "OC\\Preview\\TXT"
        "OC\\Preview\\XBitmap"
        "OC\\Preview\\HEIC" # Enable preview of HEIC/HEIF images (others are default)
        "OC\\Preview\\EMF"
      ];
      trusted_proxies = [
        # hosts.zerotier.ipv4."baldur.zto"
        # hosts.zerotier.ipv6."baldur.zto"
        hosts.tailscale.ipv4."baldur.tsn"
        hosts.tailscale.ipv6."baldur.tsn"
      ];
      trusted_domains = [ "heimdall.tsn" ];
      # overwriteprotocol = "http";
    };
    config = {
      dbtype = "pgsql";
      # extraTrustedDomains = [ "freya.tsn" ];
      adminpassFile = config.age.secrets.nextcloud_admin_secret.path;
      objectstore.s3 = {
        enable = true;
        bucket = "nextcloud";
        autocreate = true;
        key = "eHYd9AS6TLHlACBX2fC7";
        secretFile = config.age.secrets.nextcloud_bucket_secret.path;
        hostname = "localhost";
        port = 9000;
        useSsl = false;
        region = "us-east-1";
        usePathStyle = true;
      };
    };
  };

  networking.firewall.allowedTCPPorts = [ 80 ];
}
