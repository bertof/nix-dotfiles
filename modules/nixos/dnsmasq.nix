let
  blocklist = builtins.fetchurl {
    sha256 = "sha256:16xcx2z8ziv2fbqhr4ajayxblcs4i1ckrwnf50iina9asgia18za";
    url = "https://github.com/notracking/hosts-blocklists/raw/master/dnsmasq/dnsmasq.blacklist.txt";
  };
in
{
  networking.firewall.allowedTCPPorts = [ 53 ];
  networking.firewall.allowedUDPPorts = [ 53 ];

  services.dnsmasq = {
    enable = true;
    servers = [
      "1.1.1.1"
      "8.8.8.8"
      "8.8.4.4"
    ];
    extraConfig = ''
      cache-size=10000
      log-queries
      local-ttl=300

      conf-file=${blocklist}
    '';
  };
}
