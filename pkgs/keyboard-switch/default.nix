{ writeShellScriptBin
, lib
, xorg
, libnotify
, coreutils
, keyboard_name ? "AT Translated Set 2 keyboard"
,
}:
writeShellScriptBin "keyboard-switch" ''
  set -e

  PATH=$PATH:"${
    lib.makeBinPath [
      coreutils
      xorg.xinput
      libnotify
    ]
  }"
  fconfig="''${XDG_DATA_HOME:-$HOME/.local/share}/keyboard-switch.state"
  if [ ! -f $fconfig ]; then
    echo "Creating config file"
    mkdir -p `dirname $config_folder`
    echo "enabled" > $fconfig
  fi

  read -r var< $fconfig
  echo "keyboard ${keyboard_name} is $var"

  if [ "$var" = "disabled" ]; then
    notify-send "Enabling keyboard..." \ "Keyboard connected !";
    echo "enable keyboard..."
    xinput enable "${keyboard_name}"
    echo "enabled" > $fconfig
  elif [ "$var" = "enabled" ]; then
    notify-send "Disabling Keyboard" \ "Keyboard disconnected";
    echo "disable keyboard"
    xinput disable "${keyboard_name}"
    echo 'disabled' > $fconfig
  fi
''
