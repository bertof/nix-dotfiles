{ writeShellScriptBin
, lib
, wl-clipboard
, coreutils
, editor ? "hx"
, terminal-command ? "kitty -e"
,
}:
#  64   │     function clipcopy() { xclip -in -selection clipboard < "${1:-/dev/stdin}"; }
#  65   │     function clippaste() { xclip -out -selection clipboard; }  clip
writeShellScriptBin "wl-clipedit" ''
  PATH=$PATH:"${
    lib.makeBinPath [
      coreutils
      wl-clipboard
    ]
  }"
  tmp_file=$(mktemp)
  wl-paste -t 'text/plain;charset=utf-8' -n > "$tmp_file"
  ${terminal-command} ''${VISUAL:-''${EDITOR:-${editor}}} "$tmp_file"
  wl-copy -t 'text/plain;charset=utf-8' -n < "$tmp_file"
  rm "$tmp_file"
''
