{ pkgs ? import <nixos> { inherit system; }
, system ? builtins.currentSystem
,
}:
let
  self = {
    clipedit = pkgs.callPackage ./clipedit { };
    cocktail-bar-cli = pkgs.callPackage ./cocktail-bar-cli { };
    lockscreen = pkgs.callPackage ./lockscreen { };
    sddm-theme-clairvoyance = pkgs.callPackage ./sddm-theme-clairvoyance { };
    sddm-sugar-dark = pkgs.callPackage ./sddm-sugar-dark { };
    update-background = pkgs.callPackage ./update-background { };
    keyboard-switch = pkgs.callPackage ./keyboard-switch { };
  };
in
self
