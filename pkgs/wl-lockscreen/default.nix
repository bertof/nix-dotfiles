{ roboto
, swaylock
, writeShellScriptBin
, wallpaper ? ../../wallpapers/n8ca4obsys991.png
, font ? { package = roboto; name = "Roboto"; size = 10; }
, strPalette ? {
    primary.background = "000000ff";
    normal = { red = "ff0000ff"; green = "00ff00ff"; black = "000000ff"; };
    bright = { red = "ff1010ff"; green = "10ff10ff"; blue = "1010ffff"; };
  }
}: writeShellScriptBin "wl-lockscreen" ''
  # Using font package ${font.package}
  # ${builtins.toJSON strPalette}
  ${swaylock}/bin/swaylock \
  --color "${strPalette.primary.background}" \
  --inside-ver-color "${strPalette.normal.green}" \
  --inside-wrong-color "${strPalette.normal.red}" \
  --inside-color "${strPalette.normal.black}" \
  --ring-ver-color "${strPalette.bright.green}" \
  --ring-wrong-color "${strPalette.bright.red}" \
  --ring-ver-color "${strPalette.bright.green}" \
  --ring-wrong-color "${strPalette.bright.red}" \
  --ring-color "${strPalette.bright.blue}" \
  --line-uses-ring \
  --key-hl-color "${strPalette.bright.red}" \
  --bs-hl-color "${strPalette.bright.red}" \
  --separator-color "${strPalette.normal.black}" \
  --font "${font.name}" \
  --font-size "${toString font.size}" \
  --image "${toString wallpaper}" \
  --scaling fill
  # --indicator-radius 120 \
  # --indicator-thickness 15 \
''
