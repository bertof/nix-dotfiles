{ lib
, stdenv
, fetchFromGitHub
, sddm
, qt5
}:
stdenv.mkDerivation {
  pname = "sddm-sugar-dark";
  version = "2023-02-17";

  src = fetchFromGitHub {
    owner = "MarianArlt";
    repo = "sddm-sugar-dark";
    rev = "ceb2c455663429be03ba62d9f898c571650ef7fe";
    sha256 = "sha256-flOspjpYezPvGZ6b4R/Mr18N7N3JdytCSwwu6mf4owQ=";
  };

  buildInputs = [
    qt5.qtgraphicaleffects
    qt5.qtquickcontrols2
    qt5.qtsvg
    qt5.wrapQtAppsHook
    sddm
  ];

  installPhase = ''
    mkdir -p $out/usr/share/sddm/themes/
    ln -s $src $out/usr/share/sddm/themes/sugar-dark
  ''
    # + optionalString (wallpaper != null) ''
    #   cp ${wallpaper} $out/usr/share/sddm/themes/clairvoyance/Assets/Background.jpg
    # ''
  ;

  meta = {
    description = "Clairvoyance theme for SDDM";
    homepage = "https://github.com/marianarlt/sddm-sugar-dark";
    license = lib.licenses.gpl3Plus;
    maintainers = [ lib.maintainers.bertof ];
    platforms = lib.platforms.all;
  };
}
