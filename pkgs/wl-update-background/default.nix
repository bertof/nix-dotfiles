{ findutils
, coreutils
, writeShellScriptBin
, hyprland
, backgrounds_directory ? "$HOME/Immagini/Sfondi/"
}: writeShellScriptBin "wl-update-background"

  # ''
  #   set -e
  #   if [ $# -eq 0 ]; then
  #     image=`${findutils}/bin/find ${backgrounds_directory} -type f | ${gnugrep}/bin/grep -v "/\." | ${coreutils}/bin/shuf -n 1`
  #   else
  #     image="$1"
  #   fi
  #   echo "image: $image"
  #   ${swww}/bin/swww img "$image"
  # ''

  ''
    set -euo pipefail
    WALLPAPER_DIR=${backgrounds_directory}
    CURRENT_WALL=$(hyprctl hyprpaper listloaded)

    # Get a random wallpaper that is not the current one
    WALLPAPER=$(${findutils}/bin/find "$WALLPAPER_DIR" -type f ! -name "$(${coreutils}/bin/basename "$CURRENT_WALL")" | ${coreutils}/bin/shuf -n 1)

    # Apply the selected wallpaper
    ${hyprland}/bin/hyprctl hyprpaper reload ,"$WALLPAPER"
  ''
