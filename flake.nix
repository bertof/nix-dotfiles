{
  description = "bertof's system configuration";

  inputs = {
    flake-compat.url = "https://flakehub.com/f/edolstra/flake-compat/1.tar.gz";

    nixpkgs.url = "github:NixOS/nixpkgs/release-24.11";
    nixpkgs-u.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager-u = {
      url = "github:nix-community/home-manager";
      inputs = { nixpkgs.follows = "nixpkgs-u"; };
    };

    ragenix = {
      url = "github:yaxitech/ragenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    deploy-rs = {
      url = "github:serokell/deploy-rs";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    nix-rice.url = "github:bertof/nix-rice/modules";
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware";

    systems.url = "github:nix-systems/default";
    flake-parts.url = "github:hercules-ci/flake-parts";
    git-hooks-nix.url = "github:cachix/git-hooks.nix";
    # agenix-shell.url = "github:aciceri/agenix-shell"; # TODO
    # agenix-rekey.url = "github:oddlama/agenix-rekey"; # TODO
    # emanote.url = "github:srid/emanote";
  };

  outputs = inputs:
    let
      nix-config = {
        allowUnfree = true;
        extraOptions = "experimental-features = nix-command flakes";
        permittedInsecurePackages = [
          # "electron-27.3.11" # LogSeq
          # "aspnetcore-runtime-6.0.36" # Sonarr
          # "aspnetcore-runtime-wrapped-6.0.36" # Sonarr
          # "dotnet-sdk-6.0.428" # Sonarr
          # "dotnet-sdk-wrapped-6.0.428" # Sonarr
        ];
      };

      basic_module = {
        nixpkgs = {
          config = nix-config;
          overlays = [
            # packages
            inputs.self.overlays.packages
            inputs.self.overlays.overrides
          ];
        };
        nix = {
          inherit (nix-config) extraOptions;
          registry = {
            stable = { from = { id = "stable"; type = "indirect"; }; flake = inputs.nixpkgs; };
            unstable = { from = { id = "unstable"; type = "indirect"; }; flake = inputs.nixpkgs-u; };
          };
        };
      };

      # Home manager configuration
      homeManagerModules = [
        inputs.home-manager.nixosModules.default
        {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            extraSpecialArgs = {
              stable = inputs.nixpkgs;
              unstable = inputs.nixpkgs-u;
            };
          };
        }
      ];
      homeManagerUModules = [
        inputs.home-manager-u.nixosModules.default
        {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            extraSpecialArgs = {
              stable = inputs.nixpkgs;
              unstable = inputs.nixpkgs-u;
            };
          };
        }
      ];

      commonModules = [
        # Nix configuration
        basic_module

        # Nix rice
        inputs.nix-rice.modules.default
        ./modules/nixos/rice.nix

        # S3 cache read
        ./modules/nixos/s3_cache_read.nix

        # Agenix configuration
        inputs.ragenix.nixosModules.default

        # { services.userborn.enable = true; }
        ./modules/nixos/users/bertof.nix

        # Some basic defaults
        ./modules/nixos/basics

        # { age.secrets.ollama = { file = ./secrets/ollama.age; owner = "bertof"; }; }
      ];


      odinCommonModules = [
        inputs.nixos-hardware.nixosModules.common-cpu-intel
        inputs.nixos-hardware.nixosModules.common-pc-laptop
        inputs.nixos-hardware.nixosModules.common-pc-laptop-ssd
        ./instances/odin/hardware-configuration.nix
        ./instances/odin/common_configuration.nix

        # S3 cache write
        ./modules/nixos/s3_cache_write.nix
        { age.secrets.s3_odin = { file = ./secrets/s3_odin.age; owner = "bertof"; }; }

        ./modules/nixos/pro_audio.nix
        ./modules/nixos/kdeconnect.nix
        ./modules/nixos/steam.nix
        # ./modules/nixos/minio_local.nix

        ./modules/nixos/hyprland.nix
        { home-manager.users.bertof.imports = [ ./modules/hm/hyprland.nix ]; }

        ./modules/nixos/musa.nix
      ] ++ homeManagerUModules ++ [{ home-manager.users.bertof = import ./instances/odin/hm.nix; }];

      installerModules = commonModules ++ [ ./modules/nixos/installer.nix ];
    in
    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      systems = import inputs.systems;
      imports = [
        inputs.git-hooks-nix.flakeModule
      ];
      perSystem = { config, pkgs, system, ... }: {
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;
          config = nix-config;
          overlays = [
            # inputs.nix-rice.overlays.default
            inputs.self.overlays.packages
          ];
        };

        pre-commit.settings.hooks = {
          deadnix.enable = true;
          nixpkgs-fmt.enable = true;
          statix.enable = true;
        };

        devShells.default = pkgs.mkShellNoCC {
          buildInputs = [ pkgs.deploy-rs ];
          shellHook = ''
            ${config.pre-commit.installationScript}
            LOCAL_KEY = "/etc/nix/key";
          '';
        };

        formatter = pkgs.nixpkgs-fmt;

        packages = {
          inherit
            (pkgs)
            hass-color-recognizer
            keyboard-switch
            sddm-sugar-dark
            sddm-theme-clairvoyance
            wl-clipedit
            wl-lockscreen
            wl-update-background
            ;

          # Installer ISO
          install-iso = inputs.nixos-generators.nixosGenerate {
            inherit system;
            modules = installerModules;
            format = "install-iso";
          };
          # RAW base image
          raw-base-image = inputs.nixos-generators.nixosGenerate {
            inherit system;
            modules = installerModules;
            format = "raw-efi";
          };
          # VMDK base image
          vmdk-base-image = inputs.nixos-generators.nixosGenerate {
            system = "x86_64-linux";
            modules = installerModules;
            format = "vmware";
          };
          # Aarch64 base image
          aarch64-base-image = inputs.nixos-generators.nixosGenerate {
            system = "aarch64-linux";
            modules = installerModules;
            format = "sd-aarch64";
          };
          # Installer DigitalOcean
          do-image = inputs.nixos-generators.nixosGenerate {
            inherit system;
            modules = installerModules;
            format = "do";
          };

        };
      };

      flake = {
        overlays = {
          default = inputs.self.overlays.packages;

          packages = self: _super: {
            keyboard-switch = self.callPackage ./pkgs/keyboard-switch { };
            sddm-sugar-dark = self.callPackage ./pkgs/sddm-sugar-dark { };
            sddm-theme-clairvoyance = self.callPackage ./pkgs/sddm-theme-clairvoyance { };
            wl-clipedit = self.callPackage ./pkgs/wl-clipedit { };
            wl-lockscreen = self.callPackage ./pkgs/wl-lockscreen { };
            wl-update-background = self.callPackage ./pkgs/wl-update-background { };
          };

          overrides = _self: super: {
            google-chrome = super.google-chrome.override { commandLineArgs = [ "--password-store=gnome" "--force-dark-mode" ]; };
            sddm-theme-clairvoyance = super.sddm-theme-clairvoyance.override { wallpaper = ./wallpapers/background.jpg; };
            home-assistant-custom-components.smartir = super.home-assistant-custom-components.smartir.overrideAttrs
              (_attr: rec {
                version = "1.17.13";
                src = super.fetchFromGitHub {
                  owner = "smartHomeHub";
                  repo = "SmartIR";
                  rev = version;
                  hash = "sha256-fOd2MTBi0q+lBl6JYV1yGp9A42+BOD6/2SxYSBKy8fE=";
                };
                patches = [ ];
                postPatch = ''
                  sed 's/Broadlink/ZHA/' codes/climate/1946.json > codes/climate/50.json
                '';
              });
          };
        };

        nixosConfigurations = {
          thor = inputs.nixpkgs-u.lib.nixosSystem {
            system = "x86_64-linux";
            modules = commonModules ++ [
              ./instances/thor/hardware-configuration.nix
              inputs.nixos-hardware.nixosModules.common-cpu-amd
              inputs.nixos-hardware.nixosModules.common-pc-ssd
              ./instances/thor/configuration.nix

              # S3 cache write
              ./modules/nixos/s3_cache_write.nix
              { age.secrets.s3_thor = { file = ./secrets/s3_thor.age; owner = "bertof"; }; }

              # ./modules/nixos/cuda_support.nix
              # ./modules/nixos/ollama.nix
              ./modules/nixos/pro_audio.nix
              ./modules/nixos/kdeconnect.nix
              ./modules/nixos/steam.nix
              # ./modules/nixos/minio_local.nix

              ./modules/nixos/hyprland.nix
              { home-manager.users.bertof.imports = [ ./modules/hm/hyprland.nix ]; }

              ./modules/nixos/musa.nix
            ] ++ homeManagerUModules ++ [{
              home-manager.users.bertof = import ./instances/thor/hm.nix;
            }];
          };

          sif = inputs.nixpkgs-u.lib.nixosSystem {
            system = "x86_64-linux";
            modules = commonModules ++ [
              ./instances/sif/hardware-configuration.nix
              inputs.nixos-hardware.nixosModules.common-cpu-intel
              inputs.nixos-hardware.nixosModules.common-cpu-intel
              inputs.nixos-hardware.nixosModules.common-pc-ssd
              ./instances/sif/configuration.nix

              # S3 cache write
              ./modules/nixos/s3_cache_write.nix
              { age.secrets.s3_sif = { file = ./secrets/s3_sif.age; owner = "bertof"; }; }

              # ./modules/nixos/ollama.nix
              ./modules/nixos/pro_audio.nix
              ./modules/nixos/kdeconnect.nix
              ./modules/nixos/steam.nix
              # ./modules/nixos/minio_local.nix

              ./modules/nixos/hyprland.nix
              {
                home-manager.users.bertof.imports = [
                  ./modules/hm/hyprland.nix
                  ./modules/hm/swayidle.nix
                ];
              }

              ./modules/nixos/musa.nix
            ] ++ homeManagerUModules ++ [{
              home-manager.users.bertof = import ./instances/sif/hm.nix;
            }];
          };


          odin-nvidia = inputs.nixpkgs-u.lib.nixosSystem {
            system = "x86_64-linux";
            modules = commonModules ++ odinCommonModules ++ [ ./instances/odin/configuration-nvidia.nix ];
          };

          odin = inputs.nixpkgs-u.lib.nixosSystem {
            system = "x86_64-linux";
            modules = commonModules ++ [
              inputs.nixos-hardware.nixosModules.common-cpu-intel
              inputs.nixos-hardware.nixosModules.common-pc-laptop
              inputs.nixos-hardware.nixosModules.common-pc-laptop-ssd
              ./instances/odin/hardware-configuration.nix
              ./instances/odin/configuration.nix

              ./modules/nixos/server
              ./modules/nixos/steam.nix
            ] ++ homeManagerUModules ++ [{
              home-manager.users.bertof = import ./instances/odin/hm.nix;
            }];
          };

          heimdall = inputs.nixpkgs.lib.nixosSystem {
            system = "x86_64-linux";
            modules = commonModules ++ [
              inputs.nixos-hardware.nixosModules.common-cpu-amd
              inputs.nixos-hardware.nixosModules.common-gpu-amd
              inputs.nixos-hardware.nixosModules.common-pc-ssd
              ./modules/nixos/server

              ./instances/heimdall/hardware-configuration.nix
              ./instances/heimdall/configuration.nix

              ./modules/nixos/users/tiziano.nix

              ./modules/nixos/torrentbox.nix
              ./modules/nixos/minio_server.nix
              ./modules/nixos/rclone.nix
              ./modules/nixos/nextcloud.nix
              # ./modules/nixos/ntfy.nix
              # S3 cache read
              ./modules/nixos/s3_cache_read.nix
            ] ++ homeManagerModules ++ [{
              home-manager.users.bertof = import ./instances/heimdall/hm.nix;
            }];
          };

          freya = inputs.nixpkgs.lib.nixosSystem {
            system = "aarch64-linux";
            modules = commonModules ++ [
              inputs.nixos-hardware.nixosModules.raspberry-pi-4
              ({ lib, ... }: {
                boot.supportedFilesystems = lib.mkForce [
                  "btrfs"
                  "reiserfs"
                  "vfat"
                  "f2fs"
                  "xfs"
                  "ntfs"
                  "cifs"
                ];
              })
              ./modules/nixos/server

              ./instances/freya/hardware-configuration.nix
              ./instances/freya/configuration.nix

              ./modules/nixos/users/tiziano.nix

              ./modules/nixos/torrentbox.nix
              ./modules/nixos/minio_server.nix
              # ./modules/nixos/nextcloud.nix
              ./modules/nixos/ntfy.nix
              # S3 cache read
              ./modules/nixos/s3_cache_read.nix
            ] ++ homeManagerModules ++ [{
              home-manager.users.bertof = import ./instances/freya/hm.nix;
            }];
          };

          baldur = inputs.nixpkgs.lib.nixosSystem {
            system = "x86_64-linux";
            modules = commonModules ++ [
              ./modules/nixos/server
              ./instances/baldur/hardware-configuration.nix
              ./instances/baldur/configuration.nix
              # ./modules/nixos/digitalocean.nix

              ./modules/nixos/users/tiziano.nix
              # S3 cache read
              ./modules/nixos/s3_cache_read.nix
            ]
              # ++ homeManagerModules ++ [{
              #   home-manager.users.bertof = import ./instances/baldur/hm.nix;
              #   home-manager.users.tiziano = import ./instances/baldur/hm_tiziano.nix;
              # }]
            ;
          };

          loki = inputs.nixpkgs.lib.nixosSystem {
            system = "x86_64-linux";
            modules = commonModules ++ [
              ./modules/nixos/server

              inputs.nixos-hardware.nixosModules.common-cpu-intel
              inputs.nixos-hardware.nixosModules.common-pc-ssd
              ./instances/loki/hardware-configuration.nix
              ./instances/loki/configuration.nix

              ./modules/nixos/users/tiziano.nix
              # S3 cache read
              ./modules/nixos/s3_cache_read.nix
            ] ++ homeManagerModules ++ [{
              home-manager.users.bertof = import ./instances/loki/hm.nix;
              home-manager.users.tiziano = import ./instances/loki/hm_tiziano.nix;
            }];
          };
        };

        # # Deploy-rs checks
        # checks = builtins.mapAttrs (_system: deployLib: deployLib.deployChecks inputs.self.deploy) inputs.deploy-rs.lib;

        # Map nodes to Deploy-rs deployments
        deploy.nodes = {
          baldur = {
            hostname = "baldur.bertof.net";
            profiles.system = { user = "root"; path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos inputs.self.nixosConfigurations.baldur; };
          };
          freya = {
            hostname = "freya.tsn";
            profiles.system = { user = "root"; path = inputs.deploy-rs.lib.aarch64-linux.activate.nixos inputs.self.nixosConfigurations.freya; };
          };
          heimdall = {
            hostname = "heimdall.tsn";
            profiles.system = { user = "root"; path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos inputs.self.nixosConfigurations.heimdall; };
          };
          loki = {
            hostname = "loki.tsn";
            profiles.system = { user = "root"; path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos inputs.self.nixosConfigurations.loki; };
          };
          odin = {
            hostname = "odin.tsn";
            profiles.system = { user = "root"; path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos inputs.self.nixosConfigurations.odin; };
          };
          thor = {
            hostname = "thor.tsn";
            profiles.system = { user = "root"; path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos inputs.self.nixosConfigurations.thor; };
          };
        };
      };
    };
}
