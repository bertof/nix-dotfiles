let
  bertof_odin = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC3W3Btk1qtLHU69aFwseDuKU6PJMA+NxVXJXiRNhDce bertof@odin";
  bertof_thor = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKT+D5QE4TkgoKw5IvSYpvnvIIRM87RBePHce1Aaz3xJ bertof@thor";
  bertof_sif = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFWnGoScIwOUQurZx8j0Y18nxdUJ3/gNyP5vleKkS/00 bertof@sif";

  devUsers = [
    bertof_sif
    bertof_thor
    bertof_odin
  ];

  baldur = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMZKc/X9TsoN3UbEJUa0PIx96RGYoDEzDlZPZb0ctwTN";
  freya = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBclEOy4xs9yBp4RgfTf1FPeqTdERM6d6nDhnMQ3WVGI";
  heimdall = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOVemv80ODRlyufKVGJYJLNRogY9GcAXEqHr16FaYJ8I root@heimdal";
  loki = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICeomEH/27XFlOjQ/GTO2mo8qPMHTbzLIsX0dloxXfhb";
  odin = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP8bfOYmFN+KRjnAOdt9IazGeaRKm5tvGyblHD7MUhtr";
  sif = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPp/yFYCxhTVlNL3jWaJv9Z7d2RepW7l9Ze966AVibQJ root@sif";
  thor = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJbMiGx/QZ/RKgad3UNyEzgLfqRU0zBo8n0AU3s244Zw";

  systems = [
    baldur
    freya
    sif
    heimdall
    loki
    odin
    thor
  ];
in
{
  # "oauth_proxy_client_credentials.age".publicKeys = devUsers ++ systems;
  "baldur_wg_priv.age".publicKeys = devUsers ++ systems;
  "garage_bertof_baldur_key.age".publicKeys = devUsers ++ [ baldur ];
  "garage_bertof_freya_key.age".publicKeys = devUsers ++ [ freya ];
  "garage_bertof_loki_key.age".publicKeys = devUsers ++ [ loki ];
  "garage_bertof_odin_key.age".publicKeys = devUsers ++ [ odin ];
  "garage_bertof_thor_key.age".publicKeys = devUsers ++ [ thor ];
  "garage_rpc_secret.age".publicKeys = devUsers ++ systems;
  "garage_tiziano_baldur_key.age".publicKeys = devUsers ++ [ baldur ];
  "garage_tiziano_loki_key.age".publicKeys = devUsers ++ [ loki ];
  "kavita_token.age".publicKeys = devUsers ++ [ loki ];
  "nextcloud_admin_secret.age".publicKeys = devUsers ++ [ heimdall ];
  "nextcloud_bucket_secret.age".publicKeys = devUsers ++ [ heimdall ];
  "minio.age".publicKeys = devUsers ++ systems;
  "ntfy-odin.age".publicKeys = devUsers ++ [ odin ];
  "ntfy-loki.age".publicKeys = devUsers ++ [ loki ];
  "ntfy-freya.age".publicKeys = devUsers ++ [ freya ];
  "ntfy-thor.age".publicKeys = devUsers ++ [ thor ];
  "odin_wg_priv.age".publicKeys = devUsers ++ [ odin ];
  "oppo_wg_priv.age".publicKeys = devUsers ++ systems;
  "spotify_password.age".publicKeys = devUsers ++ systems;
  "thor_wg_priv.age".publicKeys = devUsers ++ [ thor ];
  "wg_psk.age".publicKeys = devUsers ++ systems;
  "s3_cache_write.age".publicKeys = devUsers ++ [ thor odin sif ];
  "s3_cache_read.age".publicKeys = devUsers ++ systems;
  "s3_odin.age".publicKeys = devUsers ++ [ odin ];
  "s3_sif.age".publicKeys = devUsers ++ [ sif ];
  "s3_thor.age".publicKeys = devUsers ++ [ thor ];
  "ollama.age".publicKeys = devUsers ++ [ sif thor ];
}
