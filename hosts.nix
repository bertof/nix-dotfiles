{
  public.ipv4 = {
    "baldur.bertof.net" = "51.195.90.205";
  };
  tailscale = {
    ipv4 = {
      "baldur.tsn" = "100.105.15.12";
      "freya.tsn" = "100.127.35.70";
      "heimdall.tsn" = "100.80.122.7";
      "loki.tsn" = "100.122.147.23";
      "odin.tsn" = "100.76.178.8";
      "thor.tsn" = "100.76.98.36";
    };
    ipv6 = {
      "baldur.tsn" = "fd7a:115c:a1e0::7e9:f0c";
      "freya.tsn" = "fd7a:115c:a1e0::f87f:2346";
      "heimdall.tsn" = "fd7a:115c:a1e0::4e01:7a07";
      "loki.tsn" = "fd7a:115c:a1e0::383a:9317";
      "odin.tsn" = "fd7a:115c:a1e0::4b4c:b208";
      "thor.tsn" = "fd7a:115c:a1e0::7ecc:6224";
    };
  };
  zerotier = {
    ipv4 = {
      "baldur.zto" = "172.23.171.70";
      "freya.zto" = "172.23.18.147";
      "heimdall.zto" = "172.23.128.245";
      "loki.zto" = "172.23.254.55";
      "odin.zto" = "172.23.219.133";
      "thor.zto" = "172.23.24.223";
      "tiziano.zto" = "172.23.110.109";
      "x3pro.zto" = "172.23.255.161";
    };
    ipv6 = {
      "baldur.zto" = "fd80:56c2:e21c:f9c7:5399:933b:abd2:a7c9";
      "freya.zto" = "fd80:56c2:e21c:f9c7:5399:93f3:ffbc:1355";
      "heimdall.zto" = "fd80:56c2:e21c:f9c7:5399:93b0:e66c:cda7";
      "loki.zto" = "fd80:56c2:e21c:f9c7:5399:93b3:aa75:fed1";
      "odin.zto" = "fd80:56c2:e21c:f9c7:5399:9379:ef39:0dd3";
      "thor.zto" = "fd80:56c2:e21c:f9c7:5399:9324:3c16:6499";
      "tiziano.zto" = "fd80:56c2:e21c:f9c7:5399:93f3:4bbb:8b38";
      "x3pro.zto" = "fd80:56c2:e21c:f9c7:5399:9379:6b02:be97";
    };
  };
}
