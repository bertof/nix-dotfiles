{ pkgs, ... }:
{
  home = {
    language.base = "it_IT.UTF-8";
    keyboard = {
      layout = "it";
      options = [
        "terminate:ctrl_alt_bksp"
        "compose:rctrl"
      ];
    };
    packages = builtins.attrValues {
      inherit (pkgs)
        gallery-dl
        procps
        wireguard-tools
        httpie
        ;
    };
  };

  imports = [
    ../../modules/hm/__basic.nix

    ../../modules/hm/helix.nix
    ../../modules/hm/kitty.nix
    ../../modules/hm/syncthing.nix
  ];

  home.stateVersion = "22.11";
}
