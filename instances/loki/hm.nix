{
  home = {
    language.base = "it_IT.UTF-8";
    keyboard = {
      layout = "it";
      options = [
        "terminate:ctrl_alt_bksp"
        "compose:rctrl"
      ];
    };
  };

  imports = [
    ../../modules/hm/__basic.nix

    # ../../modules/hm/development/cpp.nix
    # ../../modules/hm/development/data.nix
    # ../../modules/hm/development/go.nix
    # ../../modules/hm/development/javascript.nix
    # ../../modules/hm/development/latex.nix
    # ../../modules/hm/development/nix.nix
    # ../../modules/hm/development/python.nix
    # ../../modules/hm/development/rust.nix

    ../../modules/hm/helix.nix
    # ../../modules/hm/kakoune.nix
    ../../modules/hm/kitty.nix
    ../../modules/hm/megasync.nix
    ../../modules/hm/syncthing.nix

    ../../modules/hm/shell_aliases.nix
  ];

  home.stateVersion = "22.11";
}
