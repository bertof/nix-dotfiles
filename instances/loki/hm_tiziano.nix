{
  home = {
    language.base = "it_IT.UTF-8";
    keyboard = {
      layout = "it";
      options = [
        "terminate:ctrl_alt_bksp"
        "compose:rctrl"
      ];
    };
  };

  imports = [
    ../../modules/hm/__basic.nix

    ../../modules/hm/syncthing_tiziano.nix

    ../../modules/hm/shell_aliases.nix
  ];

  home.stateVersion = "23.05";
}
