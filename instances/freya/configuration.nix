{ pkgs
, lib
, config
, ...
}:
let
  hosts = import ../../hosts.nix;
in
{

  age.secrets = {
    ntfy-freya = {
      file = ../../secrets/ntfy-freya.age;
      owner = "bertof";
    };
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "it";
  };

  environment = {
    pathsToLink = [ "/share/zsh" ];
    systemPackages = builtins.attrValues { inherit (pkgs) helix tmux vim; };
  };

  hardware = {
    bluetooth = {
      enable = true;
      # package = pkgs.bluezFull;
    };
    opengl = {
      enable = true;
      driSupport = true;
    };
    raspberry-pi."4" = {
      # audio.enable = true; # AUDIO
      fkms-3d.enable = true; # GPU
    };
  };

  i18n.defaultLocale = "it_IT.UTF-8";

  networking.hostName = "freya";
  networking.firewall = {
    enable = true;
    allowPing = true;
    allowedTCPPorts = [
      # 445 # SAMBA
      # 139 # SAMBA
      # 5357 # SAMBA-WSDD
      # 8123 # HOME ASSISTANT
      8384 # SYNCTHING
      # 8385 # SYNCTHING
    ];
    allowedUDPPorts = [
      137 # SYNCTHING
      138 # SYNCTHING
      # 3702 # SAMBA-WSDD
    ];
    # extraCommands =
    #   "iptables -t raw -A OUTPUT -p udp -m udp --dport 137 -j CT --helper netbios-ns";
  };

  programs = {
    dconf.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    zsh.enable = true;
  };

  security.sudo.extraConfig = ''
    Defaults pwfeedback
  '';

  services = {
    # avahi = {
    #   enable = true;
    #   openFirewall = true;
    #   nssmdns4 = true;
    #   publish = {
    #     enable = true;
    #     addresses = true;
    #     domain = true;
    #     userServices = true;
    #     workstation = true;
    #   };
    #   extraServiceFiles.ssh = "${pkgs.avahi}/etc/avahi/services/ssh.service";
    # };
    blueman.enable = true;
    dbus.packages = [ pkgs.dconf ];
    gnome.gnome-keyring.enable = true;
    openssh = {
      enable = true;
      openFirewall = true;
    };
    # xserver = {
    #   enable = true;
    #   desktopManager.retroarch = { enable = true; package = pkgs.retroarchFull; };
    # };

    fail2ban = {
      enable = true;
      bantime-increment.enable = true;
    };
    plex = {
      enable = true;
      openFirewall = true;
      group = "users";
    };
    # jellyfin = { enable = true; openFirewall = true; group = "users"; };

    home-assistant = {
      enable = true;
      openFirewall = true;
      config = {
        default_config = { };

        homeassistant = {
          name = "Casa";
          latitude = "!secret home-latitude";
          longitude = "!secret home-longitude";
          country = "IT";
          elevation = 24;
          unit_system = "metric";
          time_zone = "Europe/Rome";
          external_url = "https://hass.bertof.net";
          allowlist_external_dirs = [ "/tmp" ];
        };

        http = {
          use_x_forwarded_for = true;
          trusted_proxies = [
            # hosts.zerotier.ipv4."baldur.zto"
            hosts.tailscale.ipv4."baldur.tsn"
            "::1"
            "127.0.0.1"
          ];
        };

        automation = "!include automations.yaml";
        scene = "!include scenes.yaml";
        alert = "!include alerts.yaml";

        # alarm_control_panel = {
        #   platform = "manual";
        #   code = "!secret alarm_code";
        #   trigger_time = 60;
        # };

        assist_pipeline = { };
        # bluetooth = { };
        camera = [
          # { platform = "local_file"; file_path = "/tmp/doods_camera_camera_cancello.jpg"; name = "File DOODS2 Camera cancello"; }
          # { platform = "local_file"; file_path = "/tmp/doods_camera_camera_vialetto.jpg"; name = "File DOODS2 Camera vialetto"; }
          # { platform = "local_file"; file_path = "/tmp/doods_camera_camera_garage.jpg"; name = "File DOODS2 Camera garage"; }
        ];
        cloud = { };
        config = { };
        # device_tracker = [{
        #   platform = "bluetooth_tracker";
        #   request_rssi = true;
        # }];
        image_processing = [
          # {
          #   platform = "doods";
          #   url = "http://localhost:8080";
          #   detector = "default";
          #   source = [
          #     { entity_id = "camera.camera_camera_cancello"; name = "DOODS2 Camera cancello"; }
          #     { entity_id = "camera.camera_camera_vialetto"; name = "DOODS2 Camera vialetto"; }
          #     { entity_id = "camera.camera_camera_garage"; name = "DOODS2 Camera garage"; }
          #   ];
          #   confidence = 60;
          #   file_out = "/tmp/doods_{{ camera_entity.split('.')[1] }}.jpg";
          #   scan_interval = 5;
          #   labels = [ "bicycle" "car" "cat" "dog" "person" "truck" ];
          # }
        ];
        # esphome = { };
        frontend = { };
        google_assistant = {
          project_id = "home-assistant-390217";
          report_state = true;
          service_account = "!include google-service-account.json";
          exposed_domains = [
            "alarm_control_panel"
            "button"
            "camera"
            "climate"
            "cover"
            "fan"
            "group"
            "humidifier"
            "input_boolean"
            "input_button"
            "input_select"
            "light"
            "lock"
            "media_player"
            "scene"
            "script"
            "select"
            "sensor"
            "switch"
            "vacuum"
          ];
        };
        history = { };
        logbook = { };
        # logger.default = "debug";
        logger.default = "info";
        # "map" = { };
        mobile_app = { };
        notify = [
          # { platform = "telegram"; name = "t_filippo"; chat_id = "!secret filippo_t_chat_id"; }
          # { platform = "telegram"; name = "t_famiglia"; chat_id = "!secret famiglia_t_chat_id"; }
        ];
        recorder.purge_keep_days = 30;
        script = "!include scripts.yaml";
        sensor = [
          # {
          #   platform = "systemmonitor";
          #   resources = [
          #     { type = "disk_use_percent"; }
          #     { type = "disk_use"; }
          #     { type = "disk_free"; }
          #     { type = "memory_use_percent"; }
          #     { type = "memory_use"; }
          #     { type = "memory_free"; }
          #     { type = "swap_use_percent"; }
          #     { type = "swap_use"; }
          #     { type = "swap_free"; }
          #     { type = "load_1m"; }
          #     { type = "load_5m"; }
          #     { type = "load_15m"; }
          #     { type = "network_in"; arg = "eno1"; }
          #     { type = "network_out"; arg = "eno1"; }
          #     { type = "throughput_network_in"; arg = "eno1"; }
          #     { type = "throughput_network_out"; arg = "eno1"; }
          #     { type = "packets_in"; arg = "eno1"; }
          #     { type = "packets_out"; arg = "eno1"; }
          #     { type = "ipv4_address"; arg = "eno1"; }
          #     { type = "ipv6_address"; arg = "eno1"; }
          #     { type = "network_in"; arg = "ztmjfdwjkp"; }
          #     { type = "network_out"; arg = "ztmjfdwjkp"; }
          #     { type = "throughput_network_in"; arg = "ztmjfdwjkp"; }
          #     { type = "throughput_network_out"; arg = "ztmjfdwjkp"; }
          #     { type = "packets_in"; arg = "ztmjfdwjkp"; }
          #     { type = "packets_out"; arg = "ztmjfdwjkp"; }
          #     { type = "ipv4_address"; arg = "ztmjfdwjkp"; }
          #     { type = "ipv6_address"; arg = "ztmjfdwjkp"; }
          #     { type = "processor_use"; }
          #     { type = "processor_temperature"; }
          #     # { type = "process"; arg = "octave-cli"; }
          #     { type = "last_boot"; }
          #   ];
          # }
        ];
        shopping_list = { };
        sun = { };
        system_health = { };
        # telegram_bot = [{
        #   # platform = "polling";
        #   platform = "webhooks";
        #   api_key = "!secret telegram_api_key";
        #   allowed_chat_ids = [
        #     "!secret filippo_t_chat_id"
        #     "!secret famiglia_t_chat_id"
        #   ];
        # }];
        tts = [
          {
            platform = "google_translate";
            language = "it";
            # tld = "it";
          }
        ];
      };
      configDir = "/var/lib/hass";
      configWritable = true;
      extraPackages = ps: with ps; [ securetar ];
      extraComponents = [
        "default_config"

        # "accuweather"
        "alert"
        "analytics"
        "androidtv"
        "androidtv_remote"
        "bayesian"
        "binary_sensor"
        "blueprint"
        "bluetooth_le_tracker"
        # "bluetooth_tracker"
        "button"
        "camera"
        "cast"
        "caldav"
        "calendar"
        "citybikes"
        "configurator"
        # "coronavirus"
        "cover"
        "default_config"
        "derivative"
        "device_automation"
        "device_sun_light_trigger"
        "device_tracker"
        "dlib_face_detect"
        "dlib_face_identify"
        "dlna_dmr"
        "dlna_dms"
        "esphome"
        "flux"
        "gdacs"
        "google"
        "google_translate"
        "group"
        "hassio"
        "jellyfin"
        "local_file"
        "media_player"
        "met"
        "meteoalarm"
        "network"
        # "nextcloud"
        "nmap_tracker"
        "open_meteo"
        "notify"
        "ping"
        "plex"
        "proximity"
        "radarr"
        "radio_browser"
        "random"
        "scene"
        "schedule"
        "script"
        "sonarr"
        "speedtestdotnet"
        "spotify"
        "tcp"
        # "telegram"
        # "telegram_bot"
        "template"
        "threshold"
        "transmission"
        "tod" # times of the day
        "trend"
        # "tuya"
        "upnp"
        "wake_on_lan"
        "wled"
        "workday"
        "zha"
        "zoneminder"
      ];
    };

    prometheus = {
      enable = true;
      exporters = {
        node = {
          enable = true;
          enabledCollectors = [ "systemd" ];
        };
        process = {
          enable = true;
          settings.process_names = [
            # Remove nix store path from process name
            {
              name = "{{.Matches.Wrapped}} {{ .Matches.Args }}";
              cmdline = [ "^/nix/store[^ ]*/(?P<Wrapped>[^ /]*) (?P<Args>.*)" ];
            }
          ];
        };
        systemd.enable = true;
      };
      globalConfig = {
        scrape_interval = "5s";
      };
      scrapeConfigs = [
        {
          job_name = "node";
          static_configs = [
            { targets = [ "localhost:${toString config.services.prometheus.exporters.node.port}" ]; }
          ];
        }
        {
          job_name = "process";
          static_configs = [
            { targets = [ "localhost:${toString config.services.prometheus.exporters.process.port}" ]; }
          ];
        }
        {
          job_name = "systemd";
          static_configs = [
            { targets = [ "localhost:${toString config.services.prometheus.exporters.systemd.port}" ]; }
          ];
        }
      ];
      retentionTime = "15d";
    };

    snapper.configs =
      let
        common = {
          TIMELINE_CREATE = true;
          TIMELINE_CLEANUP = true;
        };
      in
      {
        bertof = lib.recursiveUpdate common {
          SUBVOLUME = "/mnt/raid/bertof/";
          ALLOW_USERS = [ "bertof" ];
        };
        tiziano = lib.recursiveUpdate common {
          SUBVOLUME = "/mnt/raid/tiziano/";
          ALLOW_USERS = [ "tiziano" ];
        };
        condiviso = lib.recursiveUpdate common {
          SUBVOLUME = "/mnt/raid/condiviso";
          ALLOW_USERS = [
            "bertof"
            "tiziano"
          ];
        };
      };
  };

  time.timeZone = "Europe/Rome";

  zramSwap.enable = true;

  system.stateVersion = "22.11";
}
