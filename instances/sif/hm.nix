{ pkgs, ... }:
{
  home = {
    language.base = "it_IT.UTF-8";
    keyboard = {
      layout = "us,us,it";
      variant = ",colemak,";
      options = [
        "terminate:ctrl_alt_bksp"
        "compose:rctrl"
        "grp:menu_toggle"
      ];
    };
    packages = builtins.attrValues {
      inherit (pkgs)
        # chromium
        # discord
        # droidcam
        # element-desktop # matrix client
        # evolution
        # freecad
        # gnome-font-viewer
        # gucharmap
        # keyboard-switch
        # krita
        # minecraft
        # mycrypto
        # pcmanfm
        # retroarchFull
        # shotwell
        # signal-desktop
        # skypeforlinux
        # slack
        # transmission_4
        # wineFull
        # wireguard-tools
        authenticator
        bluetui
        brave
        eog
        evince
        file-roller
        heroic
        openvpn
        p7zip
        pavucontrol
        procps
        protonvpn-gui
        spotify
        tdesktop
        teams-for-linux
        xournalpp
        ;
    };
  };

  services = {
    gnome-keyring.enable = true;
  };

  xsession = {
    enable = true;
    numlock.enable = true;
  };

  imports = [
    ../../modules/hm/__basic.nix

    # ../../modules/hm/development/cpp.nix
    # ../../modules/hm/development/database.nix
    ../../modules/hm/development/data.nix
    ../../modules/hm/development/docker.nix
    # ../../modules/hm/development/go.nix
    # ../../modules/hm/development/javascript.nix
    ../../modules/hm/development/kubernetes.nix
    ../../modules/hm/development/latex.nix
    ../../modules/hm/development/markdown.nix
    ../../modules/hm/development/nix.nix
    ../../modules/hm/development/python.nix
    # ../../modules/hm/development/rust.nix
    ../../modules/hm/development/web.nix

    # ../../modules/hm/alacritty.nix
    # ../../modules/hm/autorandr.nix
    # ../../modules/hm/biblio.nix
    # ../../modules/hm/bitwarden.nix
    # ../../modules/hm/blender.nix
    # ../../modules/hm/carapace.nix
    # ../../modules/hm/dwarf-fortress.nix
    # ../../modules/hm/fonts.nix
    # ../../modules/hm/gnome_shell.nix
    # ../../modules/hm/grobi.nix
    # ../../modules/hm/jellyfin-player.nix
    # ../../modules/hm/joystickwake.nix
    # ../../modules/hm/kakoune.nix
    # ../../modules/hm/keepassxc.nix
    # ../../modules/hm/kicad.nix
    # ../../modules/hm/lutris.nix
    # ../../modules/hm/mangohud.nix
    # ../../modules/hm/megasync.nix
    # ../../modules/hm/nautilus.nix
    # ../../modules/hm/obs-studio.nix
    # ../../modules/hm/pycharm.nix
    # ../../modules/hm/rofi.nix
    # ../../modules/hm/screen_locker.nix
    # ../../modules/hm/security.nix
    # ../../modules/hm/spotifyd.nix
    # ../../modules/hm/thunar.nix
    # ../../modules/hm/twmn.nix
    # ../../modules/hm/update_background.nix
    # ../../modules/hm/vivaldi.nix
    # ../../modules/hm/vscode.nix
    ../../modules/hm/cava.nix
    ../../modules/hm/easyeffects.nix
    ../../modules/hm/firefox.nix
    ../../modules/hm/gtk_theme.nix
    ../../modules/hm/heif.nix
    ../../modules/hm/helix.nix
    ../../modules/hm/kdeconnect.nix
    ../../modules/hm/kitty.nix
    ../../modules/hm/libinput-gestures.nix
    ../../modules/hm/mpv.nix
    ../../modules/hm/ncspot.nix
    ../../modules/hm/noti.nix
    ../../modules/hm/nushell.nix
    ../../modules/hm/office.nix
    ../../modules/hm/office.nix
    ../../modules/hm/pass.nix
    ../../modules/hm/pro_audio.nix
    ../../modules/hm/rclone-mount.nix
    ../../modules/hm/research.nix
    ../../modules/hm/syncthing.nix
    ../../modules/hm/vim.nix
    ../../modules/hm/virtualization.nix
    ../../modules/hm/webapp.nix
    ../../modules/hm/xresources.nix
    ../../modules/hm/yazi.nix
    ../../modules/hm/zathura.nix
    ../../modules/hm/zellij.nix

  ];

  home.stateVersion = "24.11";
}
