{ config
, pkgs
, lib
, ...
}:
{
  age.secrets = {
    # ntfy-sif = {
    #   file = ../../secrets/ntfy-odin.age;
    #   owner = "bertof";
    # };
    # sif.file = ../../secrets/odin_wg_priv.age;
  };

  # Use the systemd-boot EFI boot loader.
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    # kernelPackages = pkgs.linuxPackages_6_7_hardened;
    # kernelPackages = pkgs.linuxPackages_6_8;
    kernelParams = [ ];

    # initrd.kernelModules = [ "i915" ];

    initrd.checkJournalingFS = true; # Use same ACPI identifier as Dell Ubuntu
    loader = {
      systemd-boot = {
        enable = true;
        editor = false;
      };
      efi.canTouchEfiVariables = true;
    };
    extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];

    # Cross-build arm
    binfmt.emulatedSystems = [
      "armv7l-linux"
      "aarch64-linux"
    ];
  };

  hardware = {
    logitech.wireless = { enable = true; enableGraphical = true; };

    graphics = {
      enable = true;
      enable32Bit = true;
      extraPackages = with pkgs; [ vpl-gpu-rt ];
    };

    enableRedistributableFirmware = true;


    # Tablet
    opentabletdriver = {
      enable = true;
      daemon.enable = true;
    };
  };

  networking = {
    hostName = "sif";
    networkmanager.enable = true;
    # networkmanager.wifi.backend = "iwd";

    # Configure network proxy if necessary
    # proxy.default = "http://user:password@proxy:port/";
    # proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    # Open ports in the firewall.
    # firewall.allowedTCPPorts = [ ... ];
    # firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    # firewall.enable = false;

    # wg-quick.interfaces = {
    #   wg0 = {
    #     autostart = false;
    #     address = [ "10.0.0.2/24" "fdc9:281f:04d7:9ee9::2/64" ];
    #     dns = [ "10.0.0.1" "fdc9:281f:04d7:9ee9::1" ];
    #     privateKeyFile = config.age.secrets.sif.path;

    #     peers = [
    #       {
    #         # baldur
    #         # allowedIPs = [ "10.0.0.3/32" "fdc9:281f:04d7:9ee9::3/128" ];
    #         allowedIPs = [ "0.0.0.0/0" "::/0" ];
    #         endpoint = "baldur.bertof.net:51820";
    #         presharedKeyFile = config.age.secrets.wg_psk.path;
    #         publicKey = "K57ikgFSR1O0CXWBxfQEu7uxSOsp3ePj/NMRets5pVc=";
    #       }
    #       {
    #         # sif
    #         publicKey = "LDBhvzeYmHJ0z5ch+N559GWjT3It1gZvGR/9WtCfURw=";
    #         presharedKeyFile = config.age.secrets.wg_psk.path;
    #         allowedIPs = [ "10.0.0.2/24" "fdc9:281f:04d7:9ee9::2/128" ];
    #       }
    #       {
    #         # oppo
    #         publicKey = "OBk6bHKuIYLwD7cwjmAuMn57jXqbDwCL52jhQxiHnnA=";
    #         presharedKeyFile = config.age.secrets.wg_psk.path;
    #         allowedIPs = [ "10.0.0.3/24" "fdc9:281f:04d7:9ee9::3/128" ];
    #       }
    #       {
    #         # thor
    #         publicKey = "rpwR6n4IE96VZAmQDBufsWE/a9G7d8fpkvY1OwsbOhk=";
    #         presharedKeyFile = config.age.secrets.wg_psk.path;
    #         allowedIPs = [ "10.0.0.4/24" "fdc9:281f:04d7:9ee9::4/128" ];
    #       }
    #     ];
    #   };
    # };
  };

  services = {

    # X11 windowing system.
    xserver = {
      enable = true;
      # Enable the GNOME 3 Desktop Environment.
      desktopManager.gnome.enable = true;
      # videoDrivers = [ "intel" ];
      # desktopManager.plasma5 = {
      #   enable = true;
      #   runUsingSystemd = true;
      # };
      # windowManager.bspwm.enable = true;

      xkb = {
        layout = "us,it";
        options = "eurosign:e,terminate:ctrl_alt_bksp,compose:rctrl,grp:menu_toggle";
      };

      # displayManager.gdm = {
      #   enable = true;
      #   autoSuspend = false;
      # };

    };
    displayManager = {
      enable = true;
      defaultSession = "hyprland";
    };
    libinput.enable = true;

    keybase.enable = true;

    pipewire = {
      enable = true;
      pulse.enable = true;
      jack.enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      # media-session.enable = true;
    };

    # Enable CUPS to print documents.
    printing = {
      enable = true;
      drivers = [
        pkgs.gutenprint
        # pkgs.cups-kyocera
        pkgs.cups-kyodialog
      ];
    };

    # Disable auto handling of power button
    logind = {
      powerKey = "ignore";
      lidSwitch = "hibernate";
    };
  };

  # Set your time zone.
  time.timeZone = "Europe/Rome";

  # Select internationalisation properties.
  i18n.defaultLocale = "it_IT.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  environment = {
    pathsToLink = [ "/share/zsh" ];
    sessionVariables.LD_LIBRARY_PATH = lib.mkForce "${config.services.pipewire.package.jack}/lib"; # Temporary fix for WebKitGTK
    systemPackages = builtins.attrValues {
      inherit (pkgs)
        git
        helix
        ntfs3g
        tmux
        vim
        ;
    };
  };

  programs = {
    dconf.enable = true;
    flashrom.enable = true;
    gnupg.agent = { enable = true; enableSSHSupport = true; };
    zsh.enable = true;
  };
  # services.joycond.enable = true;

  services.displayManager.sddm = {
    enable = true;
    autoNumlock = true;
    # theme = "${pkgs.catppuccin-sddm}/usr/share/sddm/themes/catppuccin-mocha";
  };

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    openFirewall = true;
  };

  # Cooling management
  services.thermald.enable = true;

  services.smartd.enable = true;
  services.snapper = {
    configs =
      let
        common = {
          ALLOW_USERS = [ "bertof" ];
          TIMELINE_CREATE = true;
          TIMELINE_CLEANUP = true;
        };
      in
      {
        home = lib.recursiveUpdate common { SUBVOLUME = "/home/"; };
        # bertof_home = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof"; };
        # bertof_music = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof/Musica"; };
        # bertof_downloads = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof/Scaricati"; };
        # bertof_images = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof/Immagini"; };
        # bertof_videos = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof/Video"; };
        # bertof_documents = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof/Documenti"; };
        # bertof_games_ssd = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof/Giochi/SSD"; };
        # # bertof_games_sata = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof/Giochi/SATA"; };
        # # bertof_games_hdd = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof/Giochi/HDD"; };
        # bertof_git = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof/Documenti/Git"; };
      };
  };

  hardware = {
    steam-hardware.enable = true;
    bluetooth = {
      enable = true;
      # package = pkgs.bluezFull;
    };
  };

  services = {
    hardware = {
      bolt.enable = true;
    };
    dbus = {
      packages = [ pkgs.dconf ];
      implementation = "broker";
    };
    gnome.gnome-keyring.enable = true;
    blueman.enable = true;
    gvfs = {
      enable = true;
      # package = lib.mkForce pkgs.gnome3.gvfs;
    };
    tumbler.enable = true;
    # tlp.enable = false;
    # avahi = {
    #   enable = true;
    #   openFirewall = true;
    #   nssmdns4 = true;
    #   publish = {
    #     enable = true;
    #     addresses = true;
    #     domain = true;
    #     userServices = true;
    #     workstation = true;
    #   };
    #   extraServiceFiles = {
    #     ssh = "${pkgs.avahi}/etc/avahi/services/ssh.service";
    #   };
    # };
    # Power-profiles
    power-profiles-daemon.enable = true;
    fprintd = {
      enable = true;
      tod = {
        enable = true;
        driver = pkgs.libfprint-2-tod1-goodix;
      };
    };
  };

  # Clamav
  # services.clamav = {
  #   daemon.enable = true;
  #   updater.enable = true;
  # };

  # services.teamviewer.enable = true;

  # Virtualisation
  virtualisation = {
    # vswitch.enable = true;
    # docker.enable = true;
    kvmgt.enable = true;
    libvirtd = {
      enable = true;
      qemu.swtpm.enable = true;
    };
    podman.enable = true;
    # virtualbox.host.enable = true;
  };
  security = {
    # FPRINTD
    rtkit.enable = true;

    sudo.extraConfig = ''
      Defaults pwfeedback
    '';

    pam.services = {
      kwallet = { fprintAuth = true; kwallet.enable = true; };
      # xscreensaver.fprintAuth = true;
      hyprlock = { fprintAuth = true; };
      # gdm = { enableGnomeKeyring = true; fprintAuth = true; };
      git = { gnupg.enable = true; kwallet.enable = true; };
    };
  };

  zramSwap.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.11"; # Did you read the comment?
}
