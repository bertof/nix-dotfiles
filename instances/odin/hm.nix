{ pkgs, ... }:
{
  home = {
    language.base = "it_IT.UTF-8";
    keyboard = {
      layout = "it,us,us";
      variant = ",,colemak";
      options = [
        "terminate:ctrl_alt_bksp"
        "compose:rctrl"
        "grp:menu_toggle"
      ];
    };
    packages = (builtins.attrValues {
      #   inherit (pkgs)
      #     # element-desktop # matrix client
      #     # evolution
      #     # freecad
      #     # krita
      #     # minecraft
      #     # mycrypto
      #     # pcmanfm
      #     # retroarchFull
      #     # shotwell
      #     # signal-desktop
      #     # slack
      #     # wineFull
      #     authenticator
      #     brave
      #     discord
      #     droidcam
      #     eog
      #     evince
      #     file-roller
      #     gallery-dl
      #     gnome-font-viewer
      #     gnome-screenshot
      #     gnome-system-monitor
      #     gucharmap
      #     inkscape
      #     keyboard-switch
      #     openvpn
      #     p7zip
      #     pavucontrol
      #     procps
      #     skypeforlinux
      #     spotify
      #     tdesktop
      #     teams-for-linux
      #     thunderbird
      #     transmission_4
      #     wireguard-tools
      #     xournalpp
      #     ;
    }) ++ [ pkgs.kitty.terminfo ];
  };


  # services.gnome-keyring.enable = true;


  # xsession = {
  #   enable = true;
  #   numlock.enable = true;
  #   initExtra = ''
  #     ## Touchpad
  #     ${pkgs.xorg.xinput}/bin/xinput set-prop 'DELL0824:00 06CB:7E92 Touchpad' 'libinput Natural Scrolling Enabled' 1
  #     ${pkgs.xorg.xinput}/bin/xinput set-prop 'DELL0824:00 06CB:7E92 Touchpad' 'libinput Tapping Enabled' 1
  #     ${pkgs.xorg.xinput}/bin/xinput set-prop 'DELL0824:00 06CB:7E92 Touchpad' 'libinput Disable While Typing Enabled' 1
  #   '';
  # };

  imports = [
    ../../modules/hm/__basic.nix

    # ../../modules/hm/development/cpp.nix
    # ../../modules/hm/development/data.nix
    # # ../../modules/hm/development/database.nix
    # ../../modules/hm/development/docker.nix
    # ../../modules/hm/development/go.nix
    # ../../modules/hm/development/javascript.nix
    # ../../modules/hm/development/kubernetes.nix
    # ../../modules/hm/development/latex.nix
    # ../../modules/hm/development/markdown.nix
    # ../../modules/hm/development/nix.nix
    # ../../modules/hm/development/python.nix
    # ../../modules/hm/development/rust.nix
    # ../../modules/hm/development/web.nix

    # # ../../modules/hm/alacritty.nix
    # ../../modules/hm/autorandr.nix
    # ../../modules/hm/biblio.nix
    # # ../../modules/hm/bitwarden.nix
    # # ../../modules/hm/blender.nix
    # # ../../modules/hm/carapace.nix
    # # ../../modules/hm/dwarf-fortress.nix
    # ../../modules/hm/easyeffects.nix
    # ../../modules/hm/firefox.nix
    # # ../../modules/hm/vivaldi.nix
    # # ../../modules/hm/fonts.nix
    # # ../../modules/hm/gnome_shell.nix
    # # ../../modules/hm/grobi.nix
    # ../../modules/hm/gtk_theme.nix
    # ../../modules/hm/heif.nix
    # ../../modules/hm/helix.nix
    # ../../modules/hm/jellyfin-player.nix
    # # ../../modules/hm/joystickwake.nix
    # # ../../modules/hm/kakoune.nix
    # ../../modules/hm/kdeconnect.nix
    # # ../../modules/hm/keepassxc.nix
    # # ../../modules/hm/kicad.nix
    # ../../modules/hm/kitty.nix
    # ../../modules/hm/libinput-gestures.nix
    # # ../../modules/hm/lutris.nix
    # ../../modules/hm/mangohud.nix
    # ../../modules/hm/megasync.nix
    # ../../modules/hm/mpv.nix
    # # ../../modules/hm/nautilus.nix
    # ../../modules/hm/ncspot.nix
    # ../../modules/hm/nix-index.nix
    # ../../modules/hm/noti.nix
    # ../../modules/hm/nushell.nix
    # ../../modules/hm/obs-studio.nix
    # ../../modules/hm/office.nix
    # ../../modules/hm/pass.nix
    # ../../modules/hm/pro_audio.nix
    # # ../../modules/hm/pycharm.nix
    # ../../modules/hm/rclone-mount.nix
    # ../../modules/hm/research.nix
    # # ../../modules/hm/rofi.nix
    # # ../../modules/hm/screen_locker.nix
    # # ../../modules/hm/security.nix
    # # ../../modules/hm/spotifyd.nix
    # ../../modules/hm/syncthing.nix
    # # ../../modules/hm/thunar.nix
    # # ../../modules/hm/twmn.nix
    # # ../../modules/hm/update_background.nix
    # ../../modules/hm/vim.nix
    # ../../modules/hm/virtualization.nix
    # ../../modules/hm/vscode.nix
    # ../../modules/hm/webapp.nix
    # ../../modules/hm/xresources.nix
    # ../../modules/hm/yazi.nix
    # ../../modules/hm/zathura.nix
    # ../../modules/hm/zellij.nix
  ];

  home.stateVersion = "24.11";
}
