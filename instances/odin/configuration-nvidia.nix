# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{ pkgs
, ...
}:
{
  boot.initrd.kernelModules = [ "i915" ];

  hardware.nvidia = {
    # modesetting.enable = true;
    # package = pkgs.linuxPackages.nvidia_x11;
    open = false;
    prime = {
      offload.enable = false;
      sync.enable = true;
      intelBusId = "PCI:0:2:0";
      nvidiaBusId = "PCI:1:0:0";
    };
  };

  programs.gamemode = {
    enable = true;
    settings = {
      custom = {
        start = "${pkgs.libnotify}/bin/notify-send 'GameMode started'";
        end = "${pkgs.libnotify}/bin/notify-send 'GameMode ended'";
      };
    };
  };

  programs.alvr = {
    enable = true;
    openFirewall = true;
  };

  services.xserver.videoDrivers = [ "nvidia" ];
}
