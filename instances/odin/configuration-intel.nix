# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{
  boot = {
    initrd.kernelModules = [ "i915" ];
    blacklistedKernelModules = [
      "nouveau"
      "nvidia"
    ];
  };

  # This runs only Intel and nvidia does not drain power.
  ##### disable nvidia for a very nice battery life.
  hardware.nvidiaOptimus.disable = true;

  services.xserver.videoDrivers = [ "intel" ];
}
