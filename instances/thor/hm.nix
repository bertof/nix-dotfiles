{ pkgs, ... }:
{
  home = {
    language.base = "it_IT.UTF-8";
    keyboard = {
      layout = "us,it";
      variant = ",colemak,";
      options = [
        "terminate:ctrl_alt_bksp"
        "compose:rctrl"
        "grp:menu_toggle"
      ];
    };
    packages = builtins.attrValues {
      inherit (pkgs)
        # chromium
        # electrum
        # element-desktop # matrix client
        # evolution
        # freecad
        # geary
        # gnome-calendar
        # gnome-sound-recorder
        # krita
        # minecraft
        # mycrypto
        # pcmanfm
        # retroarchFull
        # seahorse
        # shotwell
        # signal-desktop
        # slack
        # wineFull
        authenticator
        bluetui
        brave
        discord
        droidcam
        eog
        evince
        file-roller
        gallery-dl
        geeqie
        gnome-font-viewer
        gnome-screenshot
        gnome-system-monitor
        gucharmap
        inkscape
        openvpn
        p7zip
        pavucontrol
        procps
        protonvpn-gui
        skypeforlinux
        spotify
        tdesktop
        teams-for-linux
        thunderbird
        transmission_4
        wireguard-tools
        xournalpp
        ;
      inherit (pkgs) heroic;
    };
  };

  services = {
    gnome-keyring.enable = true;
  };

  imports = [
    ../../modules/hm/__basic.nix

    # ../../modules/hm/development/cpp.nix
    # ../../modules/hm/development/database.nix
    # ../../modules/hm/development/go.nix
    # ../../modules/hm/development/javascript.nix
    # ../../modules/hm/development/rust.nix
    ../../modules/hm/development/data.nix
    ../../modules/hm/development/docker.nix
    ../../modules/hm/development/kubernetes.nix
    ../../modules/hm/development/latex.nix
    ../../modules/hm/development/markdown.nix
    ../../modules/hm/development/nix.nix
    ../../modules/hm/development/python.nix
    ../../modules/hm/development/web.nix

    # ../../modules/hm/alacritty.nix
    # ../../modules/hm/autorandr.nix
    # ../../modules/hm/biblio.nix
    # ../../modules/hm/bitwarden.nix
    # ../../modules/hm/carapace.nix
    # ../../modules/hm/gnome_shell.nix
    # ../../modules/hm/grobi.nix
    # ../../modules/hm/jellyfin-player.nix
    # ../../modules/hm/kakoune.nix
    # ../../modules/hm/keepassxc.nix
    # ../../modules/hm/kicad.nix
    # ../../modules/hm/lutris.nix
    # ../../modules/hm/nautilus.nix
    ../../modules/hm/obs-studio.nix
    # ../../modules/hm/picom.nix
    # ../../modules/hm/pycharm.nix
    # ../../modules/hm/rofi.nix
    # ../../modules/hm/screen_locker.nix
    # ../../modules/hm/security.nix
    # ../../modules/hm/spotifyd.nix
    # ../../modules/hm/thunar.nix
    # ../../modules/hm/twmn.nix
    # ../../modules/hm/update_background.nix
    # ../../modules/hm/vivaldi.nix
    # ../../modules/hm/vscode.nix
    ../../modules/hm/blender_nvidia.nix
    ../../modules/hm/dwarf-fortress.nix
    ../../modules/hm/easyeffects.nix
    ../../modules/hm/firefox.nix
    ../../modules/hm/fonts.nix
    ../../modules/hm/gtk_theme.nix
    ../../modules/hm/heif.nix
    ../../modules/hm/helix.nix
    ../../modules/hm/joystickwake.nix
    ../../modules/hm/kdeconnect.nix
    ../../modules/hm/kitty.nix
    ../../modules/hm/mangohud.nix
    ../../modules/hm/megasync.nix
    ../../modules/hm/mpv.nix
    ../../modules/hm/ncspot.nix
    ../../modules/hm/noti.nix
    ../../modules/hm/nushell.nix
    ../../modules/hm/office.nix
    ../../modules/hm/pass.nix
    ../../modules/hm/pro_audio.nix
    ../../modules/hm/rclone-mount.nix
    ../../modules/hm/research.nix
    ../../modules/hm/syncthing.nix
    ../../modules/hm/vim.nix
    ../../modules/hm/virtualization.nix
    ../../modules/hm/webapp.nix
    ../../modules/hm/xresources.nix
    ../../modules/hm/yazi.nix
    ../../modules/hm/zathura.nix
    ../../modules/hm/zellij.nix

  ];

  home.stateVersion = "22.11";
}
