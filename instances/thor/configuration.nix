{ lib
, config
, pkgs
, ...
}:
{

  age.secrets = {
    # thor_wg_priv = {
    #   file = ../../secrets/thor_wg_priv.age;
    # };
    ntfy-thor = { file = ../../secrets/ntfy-thor.age; owner = "bertof"; };
  };

  boot = {
    kernelModules = [ "hid-nintendo" ];
    binfmt.emulatedSystems = [
      "armv7l-linux"
      "aarch64-linux"
    ];
    kernelPackages = pkgs.linuxPackages_latest;
    # kernelPackages = pkgs.linuxPackages_6_1;
    extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    initrd.checkJournalingFS = true;
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  environment = {
    pathsToLink = [ "/share/zsh" ];
    systemPackages = builtins.attrValues {
      inherit (pkgs)
        git
        helix
        tmux
        vim
        ;
    };
  };


  hardware = {
    logitech.wireless = { enable = true; enableGraphical = true; };
    nvidia = {
      # modesetting.enable = true;
      nvidiaSettings = true;
      # package = config.boot.kernelPackages.nvidiaPackages.mkDriver {
      #   version = "555.58.02";
      #
      #   sha256_64bit = "sha256-xctt4TPRlOJ6r5S54h5W6PT6/3Zy2R4ASNFPu8TSHKM=";
      #   sha256_aarch64 = lib.fakeSha256;
      #   openSha256 = lib.fakeSha256;
      #   settingsSha256 = "sha256-ZpuVZybW6CFN/gz9rx+UJvQ715FZnAOYfHn5jt5Z2C8=";
      #   persistencedSha256 = lib.fakeSha256;
      # };

      # package = config.boot.kernelPackages.nvidiaPackages.mkDriver {
      #   version = "565.77";
      #   sha256_64bit = "sha256-CnqnQsRrzzTXZpgkAtF7PbH9s7wbiTRNcM0SPByzFHw=";
      #   sha256_aarch64 = lib.fakeSha256;
      #   openSha256 = "sha256-Fxo0t61KQDs71YA8u7arY+503wkAc1foaa51vi2Pl5I=";
      #   settingsSha256 = "sha256-VUetj3LlOSz/LB+DDfMCN34uA4bNTTpjDrb6C6Iwukk=";
      #   persistencedSha256 = "sha256-E2J2wYYyRu2Kc3MMZz/8ZIemcZg68rkzvqEwFAL3fFs=";
      # };

      package = config.boot.kernelPackages.nvidiaPackages.mkDriver {
        version = "570.86.16";
        sha256_64bit = "sha256-RWPqS7ZUJH9JEAWlfHLGdqrNlavhaR1xMyzs8lJhy9U=";
        sha256_aarch64 = lib.fakeSha256;
        openSha256 = "sha256-DuVNA63+pJ8IB7Tw2gM4HbwlOh1bcDg2AN2mbEU9VPE=";
        settingsSha256 = "sha256-9rtqh64TyhDF5fFAYiWl3oDHzKJqyOW3abpcf2iNRT8=";
        persistencedSha256 = "sha256-E2J2wYYyRu2Kc3MMZz/8ZIemcZg68rkzvqEwFAL3fFs=";
      };
      open = true;
    };
    nvidia-container-toolkit.enable = true;

    graphics = { enable = true; enable32Bit = true; };
    bluetooth = {
      enable = true;
      # package = pkgs.bluezFull;
    };
    enableRedistributableFirmware = true;
    opentabletdriver = {
      enable = true;
      daemon.enable = true;
    };
  };

  i18n.defaultLocale = "it_IT.UTF-8";

  programs = {
    dconf.enable = true;
    flashrom.enable = true;
    gamemode = {
      enable = true;
      settings = {
        custom = {
          start = "${pkgs.libnotify}/bin/notify-send 'GameMode started'";
          end = "${pkgs.libnotify}/bin/notify-send 'GameMode ended'";
        };
      };
    };
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    zsh.enable = true;
  };

  networking = {
    hostName = "thor"; # Define your hostname.
    networkmanager.enable = true;

    firewall.enable = false;

    # wg-quick.interfaces = {
    #   wg0 = {
    #     autostart = false;
    #     address = [
    #       "10.0.0.4/24"
    #       "fdc9:281f:04d7:9ee9::4/64"
    #     ];
    #     dns = [
    #       "10.0.0.1"
    #       "fdc9:281f:04d7:9ee9::1"
    #     ];
    #     privateKeyFile = config.age.secrets.thor_wg_priv.path;
    #
    #     peers = [
    #       {
    #         # baldur
    #         # allowedIPs = [ "10.0.0.3/32" "fdc9:281f:04d7:9ee9::3/128" ];
    #         allowedIPs = [
    #           "0.0.0.0/0"
    #           "::/0"
    #         ];
    #         endpoint = "baldur.bertof.net:51820";
    #         presharedKeyFile = config.age.secrets.wg_psk.path;
    #         publicKey = "K57ikgFSR1O0CXWBxfQEu7uxSOsp3ePj/NMRets5pVc=";
    #       }
    #       {
    #         # odin
    #         publicKey = "LDBhvzeYmHJ0z5ch+N559GWjT3It1gZvGR/9WtCfURw=";
    #         presharedKeyFile = config.age.secrets.wg_psk.path;
    #         allowedIPs = [
    #           "10.0.0.2/24"
    #           "fdc9:281f:04d7:9ee9::2/128"
    #         ];
    #       }
    #       {
    #         # oppo
    #         publicKey = "OBk6bHKuIYLwD7cwjmAuMn57jXqbDwCL52jhQxiHnnA=";
    #         presharedKeyFile = config.age.secrets.wg_psk.path;
    #         allowedIPs = [
    #           "10.0.0.3/24"
    #           "fdc9:281f:04d7:9ee9::3/128"
    #         ];
    #       }
    #       {
    #         # thor
    #         publicKey = "rpwR6n4IE96VZAmQDBufsWE/a9G7d8fpkvY1OwsbOhk=";
    #         presharedKeyFile = config.age.secrets.wg_psk.path;
    #         allowedIPs = [
    #           "10.0.0.4/24"
    #           "fdc9:281f:04d7:9ee9::4/128"
    #         ];
    #       }
    #     ];
    #   };
    # };
  };

  time.timeZone = "Europe/Rome";

  services = {
    # avahi = {
    #   enable = true;
    #   openFirewall = true;
    #   nssmdns4 = true;
    #   publish = {
    #     enable = true;
    #     addresses = true;
    #     domain = true;
    #     userServices = true;
    #     workstation = true;
    #   };
    #   extraServiceFiles = {
    #     ssh = "${pkgs.avahi}/etc/avahi/services/ssh.service";
    #   };
    # };
    blueman.enable = true;
    # clamav = {
    #   daemon.enable = true;
    #   updater.enable = true;
    # };
    dbus = {
      packages = [ pkgs.dconf ];
      implementation = "broker";
    };
    gnome.gnome-keyring.enable = true;
    gvfs = {
      enable = true;
      # package = pkgs.gnome3.gvfs;
    };
    # joycond.enable = true;
    keybase.enable = true;
    openssh = {
      enable = true;
      openFirewall = true;
    };
    pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      jack.enable = true;
      pulse.enable = true;
      # media-session.enable = true;
    };
    power-profiles-daemon.enable = true;
    smartd.enable = true;
    snapper = {
      configs =
        let
          common = {
            ALLOW_USERS = [ "bertof" ];
            TIMELINE_CLEANUP = true;
            TIMELINE_CREATE = true;
          };
        in
        {
          bertof_home = lib.recursiveUpdate common { SUBVOLUME = "/home/bertof"; };
        };
    };
    thermald.enable = true;
    desktopManager = {
      # lomiri.enable = true;
      # cinnamon.enable = true;
      # plasma5 = { enable = true; runUsingSystemd = true; useQtScaling = true; };
      # plasma6 = { enable = true; enableQt5Integration = true; };
    };
    # displayManager.sddm = {
    #   enable = true;
    #   theme = "${pkgs.sddm-theme-clairvoyance}/usr/share/sddm/themes/clairvoyance";
    # };

    xserver = {
      enable = true;
      # desktopManager.gnome.enable = true;
      displayManager.gdm = {
        enable = true;
        autoSuspend = false;
      };
      # windowManager.bspwm = { enable = true; };
      videoDrivers = [ "nvidia" ];
      xkb = {
        layout = "us,it";
        options = "eurosign:e,terminate:ctrl_alt_bksp,compose:rctrl,grp:menu_toggle";
      };
      xrandrHeads = [
        { primary = true; output = "DP-3"; }
      ];
    };
    # gnome.gnome-remote-desktop.enable = true;
  };

  # services.teamviewer.enable = true;

  security = {
    tpm2.enable = true;
    rtkit.enable = true;
    pam.services = {
      autounlock_gnome_keyring.enableGnomeKeyring = true;
    };
    sudo.extraConfig = ''
      Defaults pwfeedback
    '';
  };

  virtualisation = {
    docker = { enable = true; };
    kvmgt.enable = true;
    libvirtd.enable = true;
    podman.enable = true;
    # virtualbox.host.enable = true;
  };

  zramSwap.enable = true;

  system.stateVersion = "22.11"; # Did you read the comment?
}
